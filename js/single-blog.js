$(document).ready(function() {

    // $('.blog-item').on('mouseenter', function () {
    //     var height = $(this).find('.blog-item-info').outerHeight();
    //
    //     if (height > 78) {
    //         $(this).find('.blog-item-text').css('height', height);
    //     }
    // });
    //
    // $('.blog-item').on('mouseleave', function () {
    //     $(this).find('.blog-item-text').css('height', '78px');
    // });

    /**
     * Smooth scrolling
     */
    $("a.scrollto").click(function() {
        var elementClick = $(this).attr("href");
        var destination = $(elementClick).offset().top;
        jQuery("html:not(:animated),body:not(:animated)").animate({
            scrollTop: destination
        }, 800);

        return false;
    });

    /**
     * Like/dislike.
     */
    var date = new Date();
    var days = 30;
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));

    $('.lp-like').click(function(){
        var likevalue = $(this).data('like');
        $(this).html('<img class="like-preloader" src="'+params.directory_uri+'/img/loader.gif" />');

        $.ajax({
            url  : params.ajaxurl,
            type:'POST',
            data: {'action': 'likes','likes': likevalue,},
            success: function(data){
                $('#lk' + likevalue).html(data);
                var likename = 'likename_' + likevalue;
                $.cookie(likename, '1', { expires: date, path: '/' });
            }
        });
    });

    /*$('.lp-dislike').click(function(){
        var dislikevalue = $(this).data('dislike');
        $(this).html('<img src="/wp-content/themes/thrmename/images/loader.gif" />');
        $.ajax({
            url: "/wp-admin/admin-ajax.php",
            type:'POST',
            data: {'action': 'lpestimate','dislikes': dislikevalue,},
            success: function(data){
                $('#dlk' + dislikevalue).html(data);
                var likename = 'likename_' + dislikevalue;
                $.cookie(likename, '1', { expires: date, path: '/' });
            },
        });
    });*/
    
});