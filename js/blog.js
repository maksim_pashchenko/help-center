$(document).ready(function() {

    /** 
     * Load more
     */
    $('.load-more').click(function() {

        var post_ids = [];
        $('.blog-item').each(function(index, el) {
            var postId = $(el).data("post-id");
            post_ids.push(postId);
        });

        data = {
            action: 'loadmore',
            post_ids: post_ids
        };

        $.ajax({
            url  : params.ajaxurl,
            data : data,
            type : 'POST',
            beforeSend: function() {
                $('.load-more').html('<img class="img-preload" src="'+params.directory_uri+'/img/load-more.gif" alt="preloader">');

            },
            complete: function() {
                $('.load-more').html('Load more');
            },
            success: function(data) {

                if (data) {
                    $(".blog-item-wrapper").append('<div class="blog-item-pair">' + data + '</div>');

                    /** 
                     * Blog items wrapper
                     */
                    // if ($(window).width() > 991) {
                    //
                    //     if (parseInt($('.blog-item-pair:last .blog-item').length) == 1 ) {
                    //         $('.blog-item-pair:last').css('padding-bottom', '571px');
                    //     } else if (parseInt($('.blog-item-pair:last .blog-item').length) == 2) {
                    //         $('.blog-item-pair:last').css('padding-bottom', '271px');
                    //     }
                    // }

                    // $(window).on('resize', function(){
                    //
                    //     if ($(window).width() > 991) {
                    //         if (parseInt($('.blog-item-pair:last .blog-item').length) == 1 ) {
                    //             $('.blog-item-pair:last').css('padding-bottom', '571px');
                    //         } else if (parseInt($('.blog-item-pair:last .blog-item').length) == 2) {
                    //             $('.blog-item-pair:last').css('padding-bottom', '271px');
                    //         }
                    //     } else {
                    //         $('.blog-item-pair:last').css('padding-bottom', '0')
                    //     }
                    // });
                }

                if (parseInt(found_posts) <= parseInt(4)) {
                    $('.load-more-wrapper').hide();
                    $('.blog-item-container').css({
                        'padding-bottom' : 0
                    })
                } else {
                    $('.load-more-wrapper').show();
                }
            }
        });
    });

    /** 
     * Blog items wrapper
     */
    var item = $(".blog-item");

    for(var i = 0; i < item.length; i+=3) {
        item.slice(i, i+3).wrapAll("<div class='blog-item-pair'></div>");
    }



    // $('.blog-item').on('mouseenter', function () {
    //     var height = $(this).find('.blog-item-info').outerHeight();
    //
    //     if (height > 78) {
    //         $(this).find('.blog-item-text').css('height', height);
    //     }
    // });
    //
    // $('.blog-item').on('mouseleave', function () {
    //     $(this).find('.blog-item-text').css('height', '78px');
    // });


});