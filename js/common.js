$(document).ready(function () {

    /**
     * Blockchain api
     */

    $(document).on('click', '.faq_yes_button', function() {
        var data = {
            action: 'increment_faq_positive_response',
            post_id: $(this).attr('data-id'),
            nonce_code : params.nonce
        };

        jQuery.post( params.ajaxurl, data, function(response) {
            // alert('Получено с сервера: ' + response);
        });
    });

    data = {
        action: 'blockchain'
    };

    $.ajax({
        url  : params.ajaxurl,
        data : data,
        type : 'GET',
        success: function(data) {

            if (data) {
                var data_obj = JSON.parse(data);
                var eth = data_obj.eth;
                var btc = data_obj.btc;
                var smt = data_obj.smt;

                $('.tp').text('$ ' + smt.rate.toFixed(3));
                $('.tpp').text('(' + smt.percent_change['24h'] + '%)');
                $('.eth').text('$ ' + eth.rate.toFixed(3));
                $('.btc').text('$ ' + btc.rate.toFixed(3));

            } else {

            }
        }
    });

    $('.header-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true
    });
    $('.load-more').click(function () {
        $('.blog-item-drop').show(400);
    });
    $('.header-lang > li').hover(function () {
        $(this).children('.header-lang-drop').stop(false, true).fadeIn(300);
    }, function () {
        $(this).children('.header-lang-drop').stop(false, true).fadeOut(300);
    });
    $('.footer-lang > li').hover(function () {
        $(this).children('.footer-lang-drop').stop(false, true).fadeIn(300);
    }, function () {
        $(this).children('.footer-lang-drop').stop(false, true).fadeOut(300);
    });



    if ($(".heateor_sss_sharing_container")[0]) {
        var social_pos = $('.heateor_sss_sharing_container').offset().top;

        $(window).scroll(function (event) {
            var scroll = $(window).scrollTop();

            if (scroll > social_pos) {
                $('.heateor_sss_sharing_container').addClass('sticky')
            } else {
                $('.heateor_sss_sharing_container').removeClass('sticky')
            }
        });
    }

    $('.header-slider').addClass('show');

});