$(document).ready(function () {
    $('.toggle-nav').on('click', function () {
        $(this).toggleClass('on');
        var $this = $(this);
        if ($this.hasClass('on')) {
            $('.logo').hide();
            $('.mobile-menu').show();
            $('.mnu').animate({
                'left': '0px',
                'width': '100%'
            }, 200);
            $('body').css('overflow', 'hidden');
        } else {
            $('.logo').show();
            $('.mobile-menu').hide();
            $('body').css('overflow', 'auto');
        }
        window.dispatchEvent(new Event('resize'));
    });

    $('.dropdown-toggle').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
       $('.dropdown-menu').toggleClass('show');
    });

    $(window).click(function() {
        $('.dropdown-menu').removeClass('show')
    });

    $('.header-tooltip-solutions').appendTo('.header-nav-wrapper .menu-item:eq(0)');
    $('.header-tooltip-resources').appendTo('.header-nav-wrapper .menu-item:eq(1)');

    var swiper_language = new Swiper ('.swiper-language', {
        navigation: {
            nextEl: '.custom-btn-next',
            prevEl: '.custom-btn-prev'
        }
    });
});