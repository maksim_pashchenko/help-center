$(document).ready(function(){

    /**
     * Button for block Team show/hide
     */
    /*$("#btn-team").click(function() {

        $('.toggle').toggle();
        $('.btn-text').toggleClass('btn-text--hide');
    });*/

    $(".open-popup-video").videoPopup({
        autoplay: true,
        width: '800px'
    });

    // Initial
    var fotoramaOptions = {
        maxwidth: '100%',
        width: '100%',
        ratio: 670/380,
        allowfullscreen: true,
        nav: 'dots',
        autoplay: 5000,
        auto: false
    };

    function updateFotoramaNavWrap(nav) {
        var cardFooterHeight = $('.card-video .card-video--footer').outerHeight();
        var navWrapHeight = 80;// $(nav).outerHeight();
        var margin = 10;
        $(nav).css({bottom: -(navWrapHeight + cardFooterHeight + margin)});
    }

    function initNavButtons(target) {
        var $goNext = $('.fotorama-go-next');
        var $goPrev = $('.fotorama-go-prev');

        $goNext.off('click');
        $goNext.on('click', function(event){
            $(target).data('fotorama').show('>');
            event.preventDefault();
        });

        $goPrev.off('click');
        $goPrev.on('click', function(event){
            $(target).data('fotorama').show('<');
            event.preventDefault();
        });
    }

    var cardVideoFooter = {
        title: '#card-video-title',
        desc: '#card-video-desc',
        country: '#card-video-country',
        date: '#card-video-date',
        share: '#card-video-share'
    };

    var $videoBlock = $('#video-fotorama');
    $videoBlock.on('fotorama:load, fotorama:show', function(e, fotorama, extra){
        updateVideoInfo(fotorama.activeFrame);
    }).fotorama(fotoramaOptions);

    var $fotorama = $videoBlock.data('fotorama');

    updateFotoramaNavWrap('.fotorama__nav-wrap');
    initNavButtons('#video-fotorama');

    $(window).on('resize', function() {
        updateFotoramaNavWrap('.fotorama__nav-wrap');
    });

    function updateVideoInfo(activeFrame) {
        $(cardVideoFooter.title).text(activeFrame.title || '');
        $(cardVideoFooter.desc).text(activeFrame.desc || '');
        $(cardVideoFooter.date).text(activeFrame.date || '');
        $(cardVideoFooter.country).text(activeFrame.country || '');
        $(cardVideoFooter.share).attr('href', activeFrame.share || '#');
    }

    $('.videos-list').on('click', 'a', function(event){
        var videoListData = $(this).data('video-list');
        var videoListJson = $.parseJSON(JSON.stringify(videoListData));
        if ($videoBlock) {

            // change active menu item
            $('.videos-list').find('li').removeClass('active');
            $(this).closest('li').addClass('active');

            var elem = $(".videos-list--item.active");
            var offset = elem.offset().left - elem.parent().offset().left;

            $('.active-tab--border').css({
               'width' :  elem.width(),
                'left' : offset
            });

            // reload items
            $fotorama.load(videoListJson);

            // update nav wrap
            updateFotoramaNavWrap('.fotorama__nav-wrap');
        }

        event.preventDefault();
    });

    /** 
     * Init first tab
     */
    $('.videos-list').find('a:first').click();

    /**
     * Create a clone of the menu, right next to original.
     */
    // $('.heateor_sss_sharing_container').addClass('original').clone().insertAfter('.heateor_sss_sharing_container').addClass('cloned').css('position','fixed').css('top','0').css('margin-top','0').css('z-index','500').removeClass('original').hide();
    //
    // scrollIntervalID = setInterval(stickIt, 10);


    // function stickIt() {
    //
    //   var orgElementPos = $('.original').offset();
    //   orgElementTop = orgElementPos.top;
    //
    //   if ($(window).scrollTop() >= (orgElementTop)) {
    //     // scrolled past the original position; now only show the cloned, sticky element.
    //
    //     // Cloned element should always have same left position and width as original element.
    //     orgElement = $('.original');
    //     coordsOrgElement = orgElement.offset();
    //     leftOrgElement = coordsOrgElement.left;
    //     widthOrgElement = orgElement.css('width');
    //     $('.cloned').css('left',leftOrgElement+'px').css('top',0).css('width',widthOrgElement).show();
    //     $('.original').css('visibility','hidden');
    //   } else {
    //     // not scrolled past the menu; only show the original menu.
    //     $('.cloned').hide();
    //     $('.original').css('visibility','visible');
    //   }
    // }



    /** 
     * Replase tag <h2> on <div> for widgets
     */
    var cat_text = $(".widget_categories .widget-title").text();
    var tag_text = $(".widget_tag_cloud .widget-title").text();
    $(".widget_categories .widget-title").replaceWith("<div>"+cat_text+"</div>");
    $(".widget_tag_cloud .widget-title").replaceWith("<div>"+tag_text+"</div>");

});