$(document).ready(function() {

    $('.select2').select2();

	$('#country').on('change', function () { 

        var selectedCountry = $(this).val();

        if (selectedCountry != '') {
            $('.select-city').show().css('display', 'inline-block');
        } else {
            $('.select-city').show().css('display', 'none');
        }

        $('#city option[value="1"]').attr('selected', 'selected');

        $("#city option").each(function () {

            if ($(this).val() != selectedCountry) {
                $(this).attr('disabled', 'disabled');
            }
            else {
                $(this).removeAttr('disabled');
            }
        });

        $('.open-vacancies .open-vacancies--list-item').hide();
	    $('.open-vacancies .open-vacancies--list-item.' + selectedCountry).show();

        $("#country").select2();
        $("#city").select2();
        $('#city').val('1').trigger('change');
    });


    $('#city').on('change', function () {   

		var current_city = $(this).find(':selected').attr('data-city');

        var selectedCity = '';
        $("#city option").each(function () { 

            if ( $(this).attr('disabled') != 'disabled' ){
                selectedCity = $(this).data("city");
            }
        });

        $('.open-vacancies .open-vacancies--list-item ').hide();
	    $('.open-vacancies .open-vacancies--list-item.' + current_city).show();
    });


	$('.open-vacancies .vacancy-title').on('click', function () {
	    $(this).toggleClass('icon-right-1');

		$(this).parent().find('.description-work').fadeToggle(500);
        $("#careers_field").val($(this).text());

        $('.wpcf7').show();
        $('.wpcf7').appendTo( $(this).parent().find('.careers-form') );
    });

	$('#resume').on('change', function (e) {
        var file_name = e.target.files[0].name;
        $('.download-file-name').append(file_name);
    });

});