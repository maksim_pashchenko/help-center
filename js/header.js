$(document).ready(function () {
    $('.toggle-nav').on('click', function () {
        $(this).toggleClass('on');
        var $this = $(this);
        window.dispatchEvent(new Event('resize'));

        if ($this.hasClass('on')) {
            $('.mobile-menu').show();
            $('.mnu').animate({
                'left': '0px',
                'width': '100%'
            }, 200);
            $('body').css('overflow', 'hidden');
            $('.toggle-nav').css('position', 'fixed');
            $('.toggle-nav').animate({
                'right': '25px',
                'top': '25px'
            }, 200);
        } else {
            $('.mobile-menu').hide();
            $('.mnu').animate({
                'left': '-250px',
                'width': '250px'
            }, 200);
            $('.toggle-nav').css({
                'position': 'absolute',
                'top' : '13px'
            });
            $('body').css('overflow', 'auto');
        }
        window.dispatchEvent(new Event('resize'));
    });

    $('.header-tooltip-solutions').appendTo('.header-nav-wrapper .menu-item:eq(0)');
    $('.header-tooltip-resources').appendTo('.header-nav-wrapper .menu-item:eq(1)');

    var swiper_language = new Swiper ('.swiper-language', {
        navigation: {
            nextEl: '.custom-btn-next',
            prevEl: '.custom-btn-prev'
        }
    });
});