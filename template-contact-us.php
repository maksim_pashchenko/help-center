<?php
/**
 * Template name: Contact us
 */

get_header('white'); ?>

    <div class="page-content">
        <div class="container">
            <div class="description-page">
                <h1><?php the_title(); ?></h1>
            </div>

            <?php echo do_shortcode( carbon_get_post_meta($post->ID, 'form_contact_us') ); ?>

            <?php get_template_part('template-parts/social', 'block'); ?>
            
        </div>
    </div>

<?php get_footer('white');
