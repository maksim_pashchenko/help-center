<?php
/**
 * market functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package market
 */

/**
 * Carbon Fields lib
 */
add_action( 'after_setup_theme', 'crb_load' );

function crb_load() {
    require_once( 'vendor/autoload.php' );
    \Carbon_Fields\Carbon_Fields::boot();
}
add_action( 'carbon_fields_register_fields', 'tenthouse_attach_theme_options' );

function tenthouse_attach_theme_options() {
    require get_template_directory() . '/inc/post-type-video.php';
    require get_template_directory() . '/inc/post-type-announcement.php';
    require get_template_directory() . '/inc/post-type-articles.php';
    require get_template_directory() . '/inc/post-type-get_started.php';

    require get_template_directory() . '/inc/custom-fields/articles.php';
    require get_template_directory() . '/inc/custom-fields/announcement.php';
    require get_template_directory() . '/inc/custom-fields/options.php';
    // require get_template_directory() . '/inc/custom-fields/home.php';
    // require get_template_directory() . '/inc/custom-fields/single.php';
    // require get_template_directory() . '/inc/custom-fields/single-careers.php';
   
    
    // require get_template_directory() . '/inc/custom-fields/blog.php';
    require get_template_directory() . '/inc/custom-fields/video.php';
    require get_template_directory() . '/inc/custom-fields/contact-us.php';
    require get_template_directory() . '/inc/custom-fields/report-bug.php';
    require get_template_directory() . '/inc/custom-fields/support.php';
    require get_template_directory() . '/inc/custom-fields/taxonomy/video-category.php';
    // require get_template_directory() . '/inc/custom-fields/taxonomy/vacancy-city.php';
    require get_template_directory() . '/inc/custom-fields/taxonomy/faq-main-tax.php';
    require get_template_directory() . '/inc/custom-fields/widget/follow.php';
    require get_template_directory() . '/inc/custom-fields/widget/newsletter.php';
    require get_template_directory() . '/inc/post-type-faq.php';
}

add_filter('carbon_fields_map_field_api_key', 'market_get_gmaps_api_key');
function market_get_gmaps_api_key($current_key){
    return 'AIzaSyDL-pHnwb1JGvfG_7wCKcOa2aDOyexT8Ws';
}

/**
 * Carbon Options WPML Support
 */
function crb_get_i18n_suffix() {
    $suffix = '';
    if ( ! defined( 'ICL_LANGUAGE_CODE' ) ) {
        return $suffix;
    }
    $suffix = '_' . ICL_LANGUAGE_CODE;
    return $suffix;
}

function crb_get_i18n_theme_option( $option_name ) {
    $suffix = crb_get_i18n_suffix();
    return carbon_get_theme_option( $option_name . $suffix );
}

/**
 * Carbon Widget init
 */
add_action( 'widgets_init', 'load_widgets' );



if (!function_exists('market_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function market_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on market, use a find and replace
         * to change 'market' to the name of your theme in all the template files.
         */
        load_theme_textdomain('market', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'header_1' => esc_html__('Header 1', 'market'),
            'header_2' => esc_html__('Header 2', 'market'),
            'subheader_1' => esc_html__('CHOOSE SOLUTION', 'market'),
            'subheader_2' => esc_html__('PRODUCT FEATURES', 'market'),
            'subheader_3' => esc_html__('USEFUL MATERIALS', 'market'),
            'subheader_4' => esc_html__('FIRST AID', 'market'),
            'footer_1' => esc_html__('Footer 1', 'market'),
            'footer_2' => esc_html__('Footer 2', 'market'),
            'footer_3' => esc_html__('Footer 3', 'market'),
            'footer_4' => esc_html__('Footer 4', 'market')
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('market_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support('custom-logo', array(
            'height' => 250,
            'width' => 250,
            'flex-width' => true,
            'flex-height' => true,
        ));
    }
endif;
add_action('after_setup_theme', 'market_setup');

add_action('get_header', 'remove_admin_login_header');
function remove_admin_login_header() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function market_content_width()
{
    // This variable is intended to be overruled from themes.
    // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters('market_content_width', 640);
}

add_action('after_setup_theme', 'market_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function market_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'market'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'market'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<p class="widget-title">',
        'after_title' => '</p>',
    ));
}

add_action('widgets_init', 'market_widgets_init');

/**
 * Enqueue scripts and styles.
 */


// function no_more_jquery(){
//     wp_deregister_script('jquery');
// }
// add_action('wp_enqueue_scripts', 'no_more_jquery');


function market_scripts() {
    //==================================================== CSS ====================================================//
    if ( !is_page_template (array('template-video-webinars.php', 'template-support.php', 'template-report-bug.php',
            'template-contact-us.php', 'template-construction-page.php', 'template-careers-page.php',
            'template-faq.php',)  ) && !is_404()  && !is_tax( 'video_cat' ) && !is_tax('faq_main_tax') && !is_singular('faq')  ) {
        wp_enqueue_style('market-style', get_stylesheet_uri());
        wp_enqueue_style('market-fonts-css', 'https://fonts.googleapis.com/css?family=Lato');
        wp_enqueue_style('market-awesome-css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
        wp_enqueue_style('market-bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css');
        wp_enqueue_style('market-slick-css', get_template_directory_uri() . '/libs/slick/slick.css');
        wp_enqueue_style('market-slick-theme-css', get_template_directory_uri() . '/libs/slick/slick-theme.css');
        wp_enqueue_style('market-fotorama-css', get_template_directory_uri() . '/css/vendors/fotorama.css');
        wp_enqueue_style('market-main-css', get_template_directory_uri() . '/css/main.css');
        wp_enqueue_style('market-header-css', get_template_directory_uri() . '/css/header.css');
        wp_enqueue_script( 'market-header-js', get_template_directory_uri() . '/js/header.js', array(), date("Ymd"), true );

    }

    if ( !is_page_template('template-home.php') ) {
        wp_enqueue_style( 'market-blog-css', get_template_directory_uri() . '/css/blog.css' );
    }

    if ( is_page_template('template-home.php') ) {
        wp_enqueue_script( 'market-main-js', get_template_directory_uri() . '/js/main.js', array(), date("Ymd"), true );
        wp_enqueue_style( 'market-youtube-popup-css', get_template_directory_uri() . '/libs/youtube-popup/video.popup.css' );
        wp_enqueue_script( 'market-youtube-popup-js', get_template_directory_uri() . '/libs/youtube-popup/video.popup.js' , array(), date("Ymd"), true);
        wp_enqueue_script( 'market-home-js', get_template_directory_uri() . '/js/home.js', array(), date("Ymd"), true );
    }

    if ( is_single() ) {
        wp_enqueue_script('market-single-blog-js', get_template_directory_uri() . '/js/single-blog.js', array(), '', true);
    }

    if ( is_page_template(array('template-video-webinars.php', 'template-support.php', 'template-report-bug.php', 'template-contact-us.php',
            'template-construction-page.php', 'template-careers-page.php', 'template-faq.php') ) || is_404() || is_tax( 'video_cat' ) || is_tax('faq_main_tax') || is_singular('faq') ) {
        wp_enqueue_style('market-video-webinars-css', get_template_directory_uri() . '/css/video-webinars.css');
        wp_dequeue_style( 'market-blog-css');

        wp_enqueue_script( 'market-header-js', get_template_directory_uri() . '/js/header-white.js', array(), date("Ymd"), true );
        wp_enqueue_script( 'market-navigation', get_template_directory_uri() . '/js/navigation.js', array(), date("Ymd"), true );
    }
    if (is_singular('faq')) {
        wp_enqueue_script('market-single-faq-js', get_template_directory_uri() . '/js/single-faq.js', array(), date("Ymd"), true );
    }

    if (is_page_template('template-video-webinars.php') || is_tax( 'video_cat' )) {
        wp_enqueue_style( 'market-youtube-popup-css', get_template_directory_uri() . '/libs/youtube-popup/video.popup.css' );
        wp_enqueue_script( 'market-youtube-popup-js', get_template_directory_uri() . '/libs/youtube-popup/video.popup.js' , array(), date("Ymd"), true);
        wp_enqueue_script( 'market-video-webinars-js', get_template_directory_uri() . '/js/video-webinars.js' , array(), date("Ymd"), true);
    }

    if (is_page_template('template-faq.php')) {
        wp_enqueue_style( 'market-youtube-popup-css', get_template_directory_uri() . '/libs/youtube-popup/video.popup.css' );
        wp_enqueue_script( 'market-youtube-popup-js', get_template_directory_uri() . '/libs/youtube-popup/video.popup.js' , array(), date("Ymd"), true);
        wp_enqueue_script( 'market-faq-js', get_template_directory_uri() . '/js/faq.js' , array(), date("Ymd"), true);

    }

    if (is_page_template('template-blog.php')  || is_archive() || is_search() ) {
         wp_enqueue_script('market-single-blog-js', get_template_directory_uri() . '/js/blog.js', array(), '', true);
    }

    if (is_page_template('template-careers-page.php') ) {
        wp_enqueue_style('market-select2-css', get_template_directory_uri() . '/libs/select2/select2.min.css');
        wp_enqueue_script('market-select2-js', get_template_directory_uri() . '/libs/select2/select2.min.js', array(), date("Ymd"), true );
        wp_enqueue_script('market-careers-js', get_template_directory_uri() . '/js/careers-page.js', array(), date("Ymd"), true );
    }

    wp_enqueue_style( 'market-fontello-css', get_template_directory_uri() . '/css/fontello/fontello.css');
    wp_enqueue_style( 'market-swiper-css', get_template_directory_uri() . '/libs/swiper/swiper.min.css' );
    wp_enqueue_script( 'market-swiper-js', get_template_directory_uri() . '/libs/swiper/swiper.min.js' );

    //==================================================== JS ====================================================//
    wp_enqueue_script( 'market-jquery-js', get_template_directory_uri() . '/js/jquery.js', array(), date("Ymd"), false);
    wp_enqueue_script( 'market-matchHeight-js', get_template_directory_uri() . '/js/vendors/jquery.matchHeight.min.js', array(), date("Ymd"), true );
    wp_enqueue_script( 'market-lazyload-js', get_template_directory_uri() . '/libs/lazy-load/lazysizes.min.js', array(), date("Ymd"), true );
    wp_enqueue_script( 'market-fotorama-js', get_template_directory_uri() . '/js/vendors/fotorama.js', array(), date("Ymd"), true );
    wp_enqueue_script( 'market-comparison-slider-js', get_template_directory_uri() . '/js/comparison-slider.js', array(), date("Ymd"), true );
    wp_enqueue_script( 'market-slickr-js', get_template_directory_uri() . '/libs/slick/slick.min.js', array(), date("Ymd"), true );
    wp_enqueue_script( 'market-cookie-js', get_template_directory_uri() . '/js/jquery.cookie.js', array(), date("Ymd"), true );
    wp_enqueue_script( 'market-common-js', get_template_directory_uri() . '/js/common.js', array(), date("Ymd"), true );
    wp_enqueue_script( 'market-navigation', get_template_directory_uri() . '/js/navigation.js', array(), date("Ymd"), true );
    wp_enqueue_script( 'market-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), date("Ymd"), true );
    wp_enqueue_script( 'market-lazyload', get_template_directory_uri() . '/libs/lazy-load/lazysizes.min.js', array(), date("Ymd"), true );

//  dd( get_template_directory_uri() . '/libs/lazy-load/lazysizes.min.js');
    wp_localize_script('market-main-js', 'params', array(
        'ajaxurl'       => admin_url('admin-ajax.php'),
        'directory_uri' => get_template_directory_uri()
    ));

    wp_localize_script('market-single-blog-js', 'params', array(
        'ajaxurl'       => admin_url('admin-ajax.php'),
        'directory_uri' => get_template_directory_uri()
    ));

    wp_localize_script('market-common-js', 'params', array(
        'ajaxurl'       => admin_url('admin-ajax.php'),
        'nonce' => wp_create_nonce('myajax-nonce'),
        'directory_uri' => get_template_directory_uri()
    ));

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'market_scripts');

function market_admin_scripts() {
    wp_enqueue_style('market-admin-css', get_template_directory_uri() . '/css/admin.css');
}

add_action('admin_enqueue_scripts', 'market_admin_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Breadcrumbs.
 */
require get_template_directory() . '/inc/breadcrumbs.php';

/**
 * Post views.
 */
require get_template_directory() . '/inc/postviews.php';

/**
 * Like/dislike.
 */
require get_template_directory() . '/inc/likes.php';

/**
 * Post type Docs.
 */
// require get_template_directory() . '/inc/post-type-docs.php';

/**
 * Post type FAQ.
 */


/**
 * Post type Video.


/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
    require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load Jetpack compatibility file.
 */ 
function market_remove_admin_bar() {

    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}
add_action('after_setup_theme', 'market_remove_admin_bar');

/**
 * SVG to upload mimes.
 */
function market_add_svg_to_upload_mimes( $upload_mimes ) {

    $upload_mimes['svg'] = 'image/svg+xml';
    $upload_mimes['svgz'] = 'image/svg+xml';
    return $upload_mimes;
}
add_filter( 'upload_mimes', 'market_add_svg_to_upload_mimes', 10, 1 );

/**
 * Hide editor on choose templates
 */
function market_hide_editor() {

    $template_file = $template_file = basename( get_page_template() );

    if( $template_file == 'template-home.php' || 
        $template_file == 'template-blog.php' || 
        $template_file == 'template-support.php' || 
        $template_file == 'template-careers-page.php' || 
        $template_file == 'template-contact-us.php' ||
        $template_file == 'template-faq.php' ||
        $template_file == 'template-report-bug.php' ){
        remove_post_type_support('page', 'editor');
    }
}
add_action( 'admin_head', 'market_hide_editor' );

/**
 * Ajax blockchain api
 */
add_action('wp_ajax_blockchain', 'market_ajax_blockchain');
add_action('wp_ajax_nopriv_blockchain', 'market_ajax_blockchain');

function market_ajax_blockchain() {

    $url  = 'https://blockchain.socialmedia.market/exchange/usd';
    $data = file_get_contents($url);

    wp_send_json( $data );

    wp_die();
}

/**
 * Ajax load more
 **/
add_action('wp_ajax_loadmore', 'market_ajax_loadmore_market');
add_action('wp_ajax_nopriv_loadmore', 'market_ajax_loadmore_market');

function market_ajax_loadmore_market() {

    $count = 1;
    $args  = array(
        'post__not_in' => $_POST['post_ids'],
        'posts_per_page' => 3,
        'post_type'      => 'post',
        'post_status'    => 'publish',
        'orderby'        => 'title',
        'order'          => 'ASC'
    );

    $query = new WP_Query($args);

    if( $query->have_posts() ) :

        while( $query->have_posts() ): $query->the_post();

            get_template_part('template-parts/blog', 'loop');

        endwhile; ?>
        <script>
            found_posts = '<?php echo $query->found_posts; ?>';
            post_ids = '<?php echo $_POST['post_ids']; ?>';
        </script>
    <?php endif; ?>

    <?php wp_reset_postdata();
    wp_die();
}

/**
 * Reorder comment fields
 */
function market_reorder_comment_fields( $fields ) {

    $new_fields = array();
    $myorder    = array('author', 'email', 'comment');

    foreach( $myorder as $key ){
        $new_fields[ $key ] = $fields[ $key ];
        unset( $fields[ $key ] );
    }

    if( $fields )
        foreach( $fields as $key => $val )
            $new_fields[ $key ] = $val;

    return $new_fields;
}
add_filter('comment_form_fields', 'market_reorder_comment_fields' );

/**
 * Fix redirect after commet
 */
function market_redirect_after_comment($location) {

    return $_SERVER["HTTP_REFERER"];
}
add_filter('comment_post_redirect', 'market_redirect_after_comment');

/**
 * Redirect if language blog not english and not russian
 */
function market_redirect_to_blog() {

    if( is_page_template('template-blog.php') || is_archive() || is_single() ){

        if ( function_exists('icl_object_id') && ICL_LANGUAGE_CODE != 'ru' && ICL_LANGUAGE_CODE != 'en') {

            wp_redirect( site_url() . '/blog', 301 ); 
            exit;
        }
    }
}
add_action('wp_enqueue_scripts', 'market_redirect_to_blog');

/**
 * Custom template comment
 */
function market_comment( $comment, $args, $depth ) { 

    if ( 'div' === $args['style'] ) {
        $tag       = 'div';
        $add_below = 'comment';
    } else {
        $tag       = 'li';
        $add_below = 'div-comment';
    }

    $classes = ' ' . comment_class( empty( $args['has_children'] ) ? '' : 'parent', null, null, false );
    ?>

    <<?php echo $tag, $classes; ?> id="comment-<?php comment_ID() ?>">
    <?php if ( 'div' != $args['style'] ) { ?>
        <div id="div-comment-<?php comment_ID() ?>" class="comment-body"><?php
    }
    $current_user = wp_get_current_user(); ?>

    <div class="comment-author">
        <?php echo get_avatar( $comment ); ?>
    </div>

    <div class="comment-wrap">
        <div class="comment-metadata">
            <div class="comment-author-time">
                <span class="comment-author-name"><?php echo $comment->comment_author; ?></span>
                <?php $comm_link = get_comment_link( $comment->comment_ID ); ?>
                <?php $d = "M d, Y"; $comment_date = get_comment_date( $d, $comment->comment_ID ); ?>
                <time class="blog-item-date" datetime="<?php echo $comment_date; ?>">
                    <?php echo $comment_date; ?>
                </time>
            </div>
            <div class="comment-edit">
                <!-- <a href="#" class="comment-delete-link">Delete </a> -->
                <!-- <a href="#" class="comment-reply-link icon-reply">Reply</a> -->
                <?php
                edit_comment_link( __( ' Edit' ), '  ', '' );

                comment_reply_link(
                    array_merge(
                        $args,
                        array(
                            'add_below' => $add_below,
                            'depth'     => $depth,
                            'max_depth' => $args['max_depth']
                        )
                    )
                ); ?>
            </div>
        </div>
        <div class="comment-content">
            <p><?php comment_text(); ?></p>
        </div>
    </div>

    <?php if ( 'div' != $args['style'] ) { ?>
        </div>
    <?php }

}

/**
 * Rewrite category routes
 */
// Post and category has same url base, so if there is no requested category
// we manually switch request to post parameters.
function market_fix_category_requests($request) {

    if(
        array_key_exists('category_name' , $request)
        && ! get_term_by('slug', basename($request['category_name']), 'category')
    ){
        if (! get_term_by('slug', basename($request['category_name']), 'post_tag')) {
            $request['name'] = basename($request['category_name']);
        } else {
            $request['tag'] = basename($request['category_name']);
        }
        unset($request['category_name']);
    }

    return $request;
}
add_filter('request', 'market_fix_category_requests');

/**
 * Breadcrumbs custom link home after
 */
function market_breadcrumbs_home_after( $false, $linkpatt, $sep, $ptype ) {

    $qo = get_queried_object();

    if( is_single() || is_archive() ){

        if (isset($_GET['post_type']) && $_GET['post_type'] == 'faq') {
            $pages = get_pages(array(
                'meta_key'   => '_wp_page_template',
                'meta_value' => 'template-faq.php'
            ));
        } elseif ( is_singular('faq') ) {
            // Remove first link 'Home'
            $pages = get_pages(array(
                'meta_key'   => '_wp_page_template',
                'meta_value' => 'template-faq.php'
            ));
        } else {
            $pages = get_pages(array(
                'meta_key'   => '_wp_page_template',
                'meta_value' => 'template-blog.php'
            ));
        }

        if (isset($pages[0])) {
            $template_id = $pages[0]->ID;
            $page = get_post( $template_id );

            return sprintf( $linkpatt, get_permalink($page), $page->post_title ) . $sep;
        }  
    }

    return $false;
}
add_action('market_breadcrumbs_home_after', 'market_breadcrumbs_home_after', 10, 4);

/**
 * Remove the slash at the end of the url
 */
$uri = preg_replace("/\?.*/i",'', $_SERVER['REQUEST_URI']);

if (strlen($uri) > 1) { 

  if (rtrim($uri,'/') != $uri && $uri != '/wp-admin/') {

    header("HTTP/1.1 301 Moved Permanently");
    header('Location: http://'.$_SERVER['SERVER_NAME'].str_replace($uri, rtrim($uri,'/'), $_SERVER['REQUEST_URI']));
    exit();    
  }
}

function parse_video_uri( $url ) {

    // Parse the url
    $parse = parse_url( $url );

    // Set blank variables
    $video_type = '';
    $video_id = '';

    // Url is http://youtu.be/xxxx
    if ( $parse['host'] == 'youtu.be' ) {

        $video_type = 'youtube';

        $video_id = ltrim( $parse['path'],'/' );

    }

    // Url is http://www.youtube.com/watch?v=xxxx
    // or http://www.youtube.com/watch?feature=player_embedded&v=xxx
    // or http://www.youtube.com/embed/xxxx
    if ( ( $parse['host'] == 'youtube.com' ) || ( $parse['host'] == 'www.youtube.com' ) ) {

        $video_type = 'youtube';

        parse_str( $parse['query']);

        $video_id = $v;

        if ( !empty( $feature ) )
            $video_id = end( explode( 'v=', $parse['query'] ) );

        if ( strpos( $parse['path'], 'embed' ) == 1 )
            $video_id = end( explode( '/', $parse['path'] ) );

    }

    // Url is http://www.vimeo.com
    if ( ( $parse['host'] == 'vimeo.com' ) || ( $parse['host'] == 'www.vimeo.com' ) ) {

        $video_type = 'vimeo';

        $video_id = ltrim( $parse['path'],'/' );

    }
    $host_names = explode(".", $parse['host'] );
    $rebuild = ( ! empty( $host_names[1] ) ? $host_names[1] : '') . '.' . ( ! empty($host_names[2] ) ? $host_names[2] : '');
    // Url is an oembed url wistia.com
    if ( ( $rebuild == 'wistia.com' ) || ( $rebuild == 'wi.st.com' ) ) {

        $video_type = 'wistia';

        if ( strpos( $parse['path'], 'medias' ) == 1 )
            $video_id = end( explode( '/', $parse['path'] ) );

    }

    // If recognised type return video array
    if ( !empty( $video_type ) ) {

        $video_array = array(
            'type' => $video_type,
            'id' => $video_id
        );

        return $video_array;

    } else {

        return false;

    }

}

/**
 * View avatar,name for logged user
 */
function market_get_user_info_islogged() {

    if (wp_get_current_user()->exists()) {
        $user = wp_get_current_user();
        echo get_avatar( $user->ID ) .'  '. $user->display_name;
    }
}
add_action( 'comment_form_before', 'market_get_user_info_islogged' );

/**
 * Pull apart OEmbed video link to get thumbnails out
 */
function get_video_thumbnail_uri( $video_uri ) {

    $thumbnail_uri = '';
    
    // determine the type of video and the video id
    $video = parse_video_uri( $video_uri );     
    
    // get youtube thumbnail
    if ( $video['type'] == 'youtube' )
        $thumbnail_uri = 'http://img.youtube.com/vi/' . $video['id'] . '/hqdefault.jpg';
    
    // get vimeo thumbnail
    if( $video['type'] == 'vimeo' )
        $thumbnail_uri = get_vimeo_thumbnail_uri( $video['id'] );
    // get wistia thumbnail
    if( $video['type'] == 'wistia' )
        $thumbnail_uri = get_wistia_thumbnail_uri( $video_uri );
    // get default/placeholder thumbnail
    if( empty( $thumbnail_uri ) || is_wp_error( $thumbnail_uri ) )
        $thumbnail_uri = ''; 
    
    //return thumbnail uri
    return $thumbnail_uri;
    
}

if( wp_doing_ajax() ){
    add_action( 'wp_ajax_increment_faq_positive_response', 'increment_faq_positive_response' );
    add_action('wp_ajax_nopriv_increment_faq_positive_response', 'increment_faq_positive_response');
}

function increment_faq_positive_response() {
    check_ajax_referer( 'myajax-nonce', 'nonce_code' );
    $post_id = intval( $_POST['post_id'] );

    $answer_amount = intval(carbon_get_post_meta($post_id,'faq_positive_response'));

    carbon_set_post_meta( $post_id, 'faq_positive_response',$answer_amount + 1  );

    wp_die();
}

/**
 * Get list countries for taxonomy city
 */
// function get_countries() {

//     $countries = get_terms( array(
//         'taxonomy'   => array( 'vacancy_country' ),
//         'hide_empty' => true
//     ) );
//     $data = array('' => 'Choose the country');

//     foreach((array)$countries as $country): 
//         $data[$country->slug] = $country->name; 
//     endforeach; 

//     return $data;

// }

/**
 * Exclude pages from WordPress Search
 */
if (!is_admin()) {

    function wpb_search_filter($query) {

        if ($query->is_search && is_search()) {
 
            $post_type = get_query_var('post_type'); 

            if ($post_type == 'faq') {
                $query->set('post_type', 'faq');
            } else {
                $query->set('post_type', 'post');
            }
        }

        return $query;
    }
    add_filter('pre_get_posts','wpb_search_filter');
}
