<?php
/**
 * Template name: Announcement
*/
get_header(); ?>

<?php 

                    // get tax and show post for this tax
                    $terms = get_terms( array(
                        'taxonomy' => 'announcement_tax',
                        'hide_empty' => true,
                    ));
                    $terms_slug = [];

                    if ( !is_wp_error($terms) ) {
                        foreach ($terms as $term) {
                            // get posts from taxonomies
                            $args = array(
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'announcement_tax',
                                        'field'    => 'slug',
                                        'terms'    => $term->slug,
                                    ),
                                ),
                                'post_type' => 'announcement',
                                'posts_per_page' => -1
                            );
                            $posts = get_posts( $args );
                            if ( $posts ) {
                            ?>

                            <div class="faq-articles-block">
                                <h3 class="faq-articles-block__title"><?php echo $term->name ?></h3>
                                <div class="faq-articles-wrapper">
                                    <ul class="faq-list">
                                     <?php foreach( $posts as $post ) { ?>
                                            <li class="faq-item">
                                                <a href="<?php echo get_the_permalink($post->ID); ?>" class="faq-item__link">
                                                    <?php echo $post->post_title ?> 

                                                    <?php if ( carbon_get_post_meta( $post->ID, 'faq_video_oembed' ) ) : ?>
                                                        <i class="fas fa-video icon-videocam"></i>
                                                    <?php endif; ?>
                                                </a>
                                            </li>
                                     <?php } ?>

                                    </ul>
                                 </div>
                            </div>
                           <?php
                            }//end if empty
                        } //end term foreach
                     } // endif ?>

<?php get_footer();