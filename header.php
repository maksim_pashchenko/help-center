<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Market
 */
?>

<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/favicon/manifest.json">
        <link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/favicon/safari-pinned-tab.svg" color="#b31f7a">
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon.ico">
        <meta name="msapplication-config" content="<?php echo get_template_directory_uri(); ?>/favicon/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="default">

        <?php wp_head(); ?>
    </head>

<body <?php body_class(); ?>>

    <header class="section-header">
        <div class="container">
<!--            --><?php //get_template_part('template-parts/tokens'); ?>
        </div>
    </header>

    <style>
        .header-logo .st0,
        .header-logo .st1,
        .header-logo .st2,
        .header-logo .st3 {
            fill: #fff !important;
        }
    </style>

    <?php if (!is_page_template('template-home.php')) { ?>

        <div class="mobile-menu">
            <div class="mnu">

                <?php //get_template_part('template-parts/tokens'); ?>

                <?php if (has_nav_menu('header_2')) { 
                    wp_nav_menu(array(
                        'theme_location' => 'header_2',
                        'container' => false,
                        'items_wrap' => ' <nav class="header-nav">%3$s</nav>',
                    ));
                } ?>
                <div class="swiper-language swiper-container">
                    <div class="swiper-wrapper">
                        <?php $languages = icl_get_languages('skip_missing=1'); ?>
                        <?php foreach ($languages as $language) : ?>

                            <div class="swiper-slide" data-language="<?php echo $language['code'] ?>">
                                <a href="<?php echo $language['url'] ?>"><span><?php echo $language['native_name'] ?></span></a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <!-- /.swiper-wrapper -->
                    <div class="custom-btn custom-btn-prev"></div>
                    <div class="custom-btn custom-btn-next"></div>
                </div>
                <!-- /.swiper-container -->
            </div>
        </div>

        <?php if (is_page_template('template-blog.php') || is_archive() && !is_search()) {

            if (!empty(carbon_get_theme_option('bg_slider_blog'))) {
                $bg_url = carbon_get_theme_option('bg_slider_blog');
            } else {
                $bg_url = get_template_directory_uri() . '/img/default-bg-slider.jpg';
            }
        } elseif (is_search()) {

            if (!empty(carbon_get_theme_option('bg_header_search'))) {
                $bg_url = carbon_get_theme_option('bg_header_search');
            } else {
                $bg_url = get_template_directory_uri() . '/img/default-bg-slider.jpg';
            }
        } elseif (is_single()) {

            if (!empty(carbon_get_the_post_meta('bg_header_single'))) {
                $bg_url = carbon_get_the_post_meta('bg_header_single');
            } else {
                $bg_url = get_template_directory_uri() . '/img/default-bg-slider.jpg';
            }
        } else {
            $bg_url = get_template_directory_uri() . '/img/default-bg-slider.jpg';
        } ?>

        <!-- Header -->
        <section class="header">
            <div class="header-main-top header-main"
                 style="background: url(<?php echo $bg_url; ?>) no-repeat top;">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-12 col-xl-3">
                            <a href="#" class="toggle-nav"><span></span></a>
                            <a href="<?php echo esc_url(home_url('/')); ?>" class="header-logo">
                                <?php $svg_file = file_get_contents(wp_get_attachment_image_url(carbon_get_theme_option('logo_header'), 'full')); ?>
                                <?php echo $svg_file; ?>
                            </a>
                        </div>
                        <div class="col-12 col-xl-9">
                            <div class="header-nav-wrapper">
                                <nav class="header-nav">
                                    <?php if (has_nav_menu('header_1')) { 
                                        wp_nav_menu(array(
                                            'theme_location' => 'header_1',
                                            'container' => false,
                                            'items_wrap' => '<ul class="list-unstyled">%3$s</ul>',
                                        ));
                                    } ?>

                                    <?php echo market_switcher_languages('header'); ?>

                                    <?php get_template_part('template-parts/sub', 'menu'); ?>

                                </nav>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">

                            <?php if (is_page_template('template-blog.php') || is_archive() && !is_search()) {
                                get_template_part('template-parts/blog/slider', 'blog');
                            } ?>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php } ?>
