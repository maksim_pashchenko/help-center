<form class="blog-search" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ) ?>" >
	<input placeholder="<?php echo __('Search the blog', 'market'); ?>" type="text" value="<?php echo get_search_query() ?>" name="s" id="s" />
</form>