<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package market
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
} ?>

<div id="comments" class="write-comment">

	<?php
	if ( have_comments() ) :
		
		wp_list_comments('type=comment&callback=market_comment'); 

		the_comments_navigation();

		if ( ! comments_open() ) :
			?>
			<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'market' ); ?></p>
			<?php
		endif;

	endif;

	$commenter = wp_get_current_commenter();

	$args = array(
	    'title_reply'          => have_comments() ? __( 'Leave A Comment', 'market' ) : '',
	    'title_reply_to'       => __( 'Leave a comment on %s', 'market' ),
	    'title_reply_before'   => '',
	    'title_reply_after'    => '',
	    'class_form'           => 'form-block',
	    'comment_notes_after'  => '',
	    'comment_notes_before' => '',
	    'fields'               => array(
	        'author' => '<div class="form-item-pair">'.
	                        '<input type="text" id="author" name="author" value="'.esc_attr( $commenter['comment_author'] ).'" size="30" aria-required="true" placeholder="'.__('Your name *', 'market').'" required>',
	        'email'  => 	'<input id="email" name="email" type="email" value="'.esc_attr( $commenter['comment_author_email'] ).'" size="30" aria-required="true" placeholder="'.__('Your email *', 'market').'" required>'.
	                    '</div>',
	    ),
	    'label_submit'  => __('Post comment', 'market'),
	    'class_submit'  => 'btn-primary-green-gradient',
	    'logged_in_as'  => '',
	    'comment_field' => '<textarea id="comment" cols="30" rows="10" placeholder="'.__('Comment *', 'market').'" name="comment" aria-required="true" required></textarea>'
	);

	comment_form($args); ?>

</div><!-- #comments -->
