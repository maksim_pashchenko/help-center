<div class="header-tooltip header-tooltip-solutions">
    <div class="head-tooltip">
        <p><?php echo __('CHOOSE SOLUTION', 'market'); ?></p>
        <p><?php echo __('PRODUCT FEATURES', 'market'); ?></p>
    </div>
    <!-- /.head-tooltip -->

    <div class="content-tooltip-wrap">
        <div class="content-tooltip">

        <?php if (has_nav_menu('subheader_1')) {

            $menuLocations = get_nav_menu_locations(); 
            $menuID = $menuLocations['subheader_1']; // Get the *primary* menu ID
            $primaryNav = wp_get_nav_menu_items($menuID);

            foreach ( $primaryNav as $navItem ) { 
                $class = '';
                foreach ($navItem->classes as $classes) {
                    $class .= $classes . ' ';
                 } ?>

                <a href="<?php echo $navItem->url; ?>" class="content-tooltip-item <?php echo $class; ?>">
                    <p><?php echo $navItem->title; ?></p>
                    <span class="content-category"><?php echo $navItem->post_content; ?></span>
                    <span class="learn-more icon-right"><?php echo __('learn more', 'market'); ?></span>
                </a>

            <?php } ?>

        <?php } ?>

        </div>
        <div class="content-tooltip">

            <?php if (has_nav_menu('subheader_2')) {

                $menuLocations = get_nav_menu_locations(); 
                $menuID = $menuLocations['subheader_2']; // Get the *primary* menu ID
                $primaryNav = wp_get_nav_menu_items($menuID);

                foreach ( $primaryNav as $navItem ) { 
                    $class = '';
                    foreach ($navItem->classes as $classes) {
                        $class .= $classes . ' ';
                     } ?>

                    <a href="<?php echo $navItem->url; ?>" class="content-tooltip-item <?php echo $class; ?>">
                        <p><?php echo $navItem->title; ?></p>
                        <span class="content-category"><?php echo $navItem->post_content; ?></span>
                        <span class="learn-more icon-right"><?php echo __('learn more', 'market'); ?></span>
                    </a>

                <?php } ?>
                
            <?php } ?>

        </div>
    </div>
</div>
<!-- /.header-tooltip -->
<div class="header-tooltip header-tooltip-resources">
    <div class="head-tooltip">
        <p><?php echo __('USEFUL MATERIALS', 'market'); ?></p>
        <p><?php echo __('first aid', 'market'); ?></p>
    </div>
    <!-- /.head-tooltip -->

    <div class="content-tooltip-wrap">
        <div class="content-tooltip">

            <?php if (has_nav_menu('subheader_3')) {

                $menuLocations = get_nav_menu_locations(); 
                $menuID = $menuLocations['subheader_3']; // Get the *primary* menu ID
                $primaryNav = wp_get_nav_menu_items($menuID);

                foreach ( $primaryNav as $navItem ) { 
                    $class = '';
                    foreach ($navItem->classes as $classes) {
                        $class .= $classes . ' ';
                     } ?>

                    <a href="<?php echo $navItem->url; ?>" class="content-tooltip-item <?php echo $class; ?>">
                        <p><?php echo $navItem->title; ?></p>
                        <span class="content-category"><?php echo $navItem->post_content; ?></span>
                        <span class="learn-more icon-right"><?php echo __('learn more', 'market'); ?></span>
                    </a>

                <?php } ?>
                
            <?php } ?>

        </div>
        <div class="content-tooltip">

            <?php if (has_nav_menu('subheader_4')) {

                $menuLocations = get_nav_menu_locations(); 
                $menuID = $menuLocations['subheader_4']; // Get the *primary* menu ID
                $primaryNav = wp_get_nav_menu_items($menuID);

                foreach ( $primaryNav as $navItem ) { 
                    $class = '';
                    foreach ($navItem->classes as $classes) {
                        $class .= $classes . ' ';
                     } ?>

                    <a href="<?php echo $navItem->url; ?>" class="content-tooltip-item <?php echo $class; ?>">
                        <p><?php echo $navItem->title; ?></p>
                        <span class="content-category"><?php echo $navItem->post_content; ?></span>
                        <span class="learn-more icon-right"><?php echo __('learn more', 'market'); ?></span>
                    </a>

                <?php } ?>
                
            <?php } ?>

        </div>
    </div>
    <!-- /.content-tooltip-wrap -->

</div>
<!-- /.header-tooltip -->