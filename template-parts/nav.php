<div class="mobile-menu">
    <div class="mnu">

        <?php if (has_nav_menu('header_2')) {
            wp_nav_menu(array(
                'theme_location' => 'header_2',
                'container' => false,
                'items_wrap' => '<nav class="header-nav">%3$s</nav>',
            ));
        } ?>
        <?php //echo market_switcher_languages('header'); ?>

        <div class="swiper-language swiper-container">
            <div class="swiper-wrapper">
                <?php $languages = icl_get_languages('skip_missing=1'); ?>
                <?php foreach ($languages as $language) : ?>

                    <div class="swiper-slide" data-language="<?php echo $language['code'] ?>">
                        <a href="<?php echo $language['url'] ?>"><span><?php echo $language['native_name'] ?></span></a>
                    </div>
                <?php endforeach; ?>
            </div>
            <!-- /.swiper-wrapper -->
            <div class="custom-btn custom-btn-prev"></div>
            <div class="custom-btn custom-btn-next"></div>
        </div>
        <!-- /.swiper-container -->
    </div>
</div>
<section class="header-main-top home-header-section">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-xl-3">
                <a href="#" class="toggle-nav"><span></span></a>
                <a href="<?php echo esc_url(home_url('/')); ?>" class="header-logo">
                    <?php $svg_file = file_get_contents(wp_get_attachment_image_url(carbon_get_theme_option('logo_header'), 'full')); ?>
                    <?php echo $svg_file; ?>
                </a>
            </div>
            <div class="col-12 col-xl-9">
                <div class="hidden-xs">
                    <div class="header-nav-wrapper">
                        <nav class="header-nav">
                            <?php if (has_nav_menu('header_1')) {
                                wp_nav_menu(array(
                                    'theme_location' => 'header_1',
                                    'container' => false,
                                    'items_wrap' => '<ul class="list-unstyled">%3$s</ul>',
                                ));
                            } ?>
                            <?php echo market_switcher_languages('header'); ?>

                            <?php get_template_part('template-parts/sub', 'menu'); ?>

                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>