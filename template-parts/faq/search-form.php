<section class="faq">
    <div class="container">
        <div class="faq-item-container">
            <h2 class="page-title"><?php echo __('Frequently Asked Questions', 'market'); ?></h2>
            <div class="faq-help-form-wrapper">

                <form class="faq-help-form" role="search" action="<?php echo site_url('/'); ?>" method="get" id="searchform">
                    <input type="text" name="s" placeholder="<?php echo __('How can we help?', 'market'); ?>"/>
                    <input type="hidden" name="post_type" value="faq"/>
                </form>

                <p class="faq-help-form-queries">
                    <b><?php echo crb_get_i18n_theme_option( 'title_search_faq' ); ?></b>

                    <?php
                    $i = 1; 
                    $items = crb_get_i18n_theme_option( 'items_search_faq' ); 

                    foreach($items as $item): ?>

                        <a href="<?php echo get_the_permalink($item['id']); ?>">
                            <?php echo get_the_title($item['id']); ?>
                        </a>
                        <?php if(count($items) != $i): 
                            echo ","; 
                        endif; ?>

                    <?php ++$i; endforeach; ?>

                </p>
            </div>
        </div>
    </div>
</section>