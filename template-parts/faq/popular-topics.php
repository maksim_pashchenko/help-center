<section class="popular-topics">
    <div class="container">
        <div class="popular-topics-item-container">
            <h3 class="popular-topics-title"><?php echo __('Popular Topics', 'market'); ?></h3>
            <ul class="popular-topics-list">

                <?php
                global $faq_page;
                $faq_association = carbon_get_post_meta($faq_page->ID, 'faq_association');

                foreach ($faq_association as $post) {
                    $faq = get_post($post['id']);
                    $post_video = carbon_get_post_meta($post['id'], 'faq_video_oembed');
                    ?>

                    <li class="popular-topics-item">
                        <a class="popular-topics-item__link" href="<?php echo get_the_permalink($post['id']); ?>">
                            <span><?php echo $faq->post_title; ?></span>
                        </a>
                        <a class="open-popup-video" video-url="<?php echo $post_video; ?>">
                            <?php if ($post_video) { ?>
                                <span class="icon-videocam"><?php echo __('play video', 'market'); ?></span>
                            <?php } ?>
                        </a>
                    </li>

                <?php }
                ?>
            </ul>
        </div>
    </div>
</section>