<section class="faq-question">
    <div class="container">
        <div class="faq-question-list">
            <?php
            $terms = get_terms(array(
                'taxonomy' => 'faq_main_tax',
                'hide_empty' => true,
            ));
            if (!is_wp_error($terms)) {
                foreach ($terms as $term) { 
                    $color = carbon_get_term_meta( $term->term_id, 'color_faq_main_tax' ); 
                    $icon  = carbon_get_term_meta( $term->term_id, 'icon_faq_main_tax' ); ?>

                    <a href="<?php echo get_term_link($term) ?>" class="faq-question-item">
                        <div id="icon_<?php echo $term->term_id; ?>" class="faq-question-item__content">

                            <?php if ( !empty($icon) && get_post_mime_type($icon) == 'image/svg+xml' ) : 

                                if ( !empty($color) ) :
                                    echo '<style>#icon_'.$term->term_id.' path {fill: '.$color.';}</style>';
                                endif;

                                $svg_file = file_get_contents(wp_get_attachment_image_url($icon, 'full'));
                                echo $svg_file;

                            elseif ( !empty($icon) && get_post_mime_type($icon) == 'image/png' ) : 

                                echo '<img src="'.wp_get_attachment_image_url($icon, 'full').'" alt="icon">';

                            endif; ?>

                            <h3 class="faq-question-item__title"><?php echo $term->name ?></h3>
                            <p class="faq-question-item__text"><?php echo $term->description ?></p>
                        </div>
                    </a>
                    
                <?php }
            }
            ?>

            <div class="faq-question-item faq-question-item--end">
                <div class="faq-question-item__content">
                    <h3 class="popular-topics-title faq-question-item__title"><?php echo __('Can’t find your issue?', 'market'); ?></h3>
                    <span onclick="parent.LC_API.open_chat_window({source:'minimized'}); return false" class="faq-question-item__btn"><?php echo __('Contact manager', 'market'); ?></span>
                </div>
            </div>
        </div>
    </div>
</section>