<div class="send-message-social">
    <div class="send-message social-item social-item-left">
        <p><?php echo crb_get_i18n_theme_option('title_1_soc_block'); ?></p>
        <div class="send-btn-wrap">
            <a href="<?php echo crb_get_i18n_theme_option('telegramm_soc_block'); ?>" class="btn telegram-btn" target="_blank">
                <i class="icon-paper-plane"></i>
                Telegram</a>
            <a class="btn livechat-btn" onclick="parent.LC_API.open_chat_window({source:'minimized'}); return false">
                <img src="<?php echo get_template_directory_uri(); ?>/img/icon-livechat.png" alt="image">
                LiveChat</a>
        </div>
        <!-- /.send-btn-wrap -->
        <!-- /.send-messasge-telegram -->
    </div>
    <!-- /.send-message -->
    <div class="social-links social-item social-item-right">
        <p><?php echo crb_get_i18n_theme_option('title_2_soc_block'); ?></p>

        <!-- Social links starts -->
        <?php $icons = carbon_get_theme_option('rep_social_soc_block'); ?>
        <div class="row no-gutters text-center">

        <?php foreach($icons as $icon): ?>

            <?php if ($icon['url_rep_social_soc_block'] && $icon['icons_rep_social_soc_block']['class']):  ?>

                <div class="col mb-2">
                    <a rel="nofollow" target="_blank" href="<?php echo $icon['url_rep_social_soc_block']; ?>">
                        <i class="<?php echo $icon['icons_rep_social_soc_block']['class']; ?>" aria-hidden="true"></i>
                    </a>
                    <!-- noindex -->
                </div>

            <?php endif; 
        endforeach; ?>

        </div>
        <!-- Social links ends -->
    </div>
    <!-- /.social-links -->
</div>
<!-- /.send-message-social -->