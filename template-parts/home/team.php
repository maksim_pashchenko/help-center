<!-- Section: Team -->
<section class="section-team py-5">
    <div class="container">
        <h2 class="section-title text-center mb-5 font-weight-normal"><?php echo carbon_get_post_meta($post->ID, 'title_team_home'); ?></h2>

        <?php $blocks = carbon_get_post_meta($post->ID, 'rep_team_home');
        $count = 1; ?>

        <ul class="team-list">

            <?php foreach ((array)$blocks as $block): 

                if (!empty($block['url_rep_team_home']) ||
                    !empty($block['img_rep_team_home']) ||
                    !empty($block['name_url_rep_team_home']) ||
                    !empty($block['job_rep_team_home']) ||
                    !empty($block['desc_rep_team_home'])
                ): ?>

                <li class="team-item" data-mh="team-item">

                    <div class="team-item--inner">
                        <div class="team-item--link">
                            <?php if (!empty($block['url_rep_team_home'])): ?>
                                <a target="_blank" href="<?php echo $block['url_rep_team_home']; ?>">
                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                </a>
                            <?php endif; ?>
                        </div>
                        <div class="media">
                            <div class="media-element fl">
                                <?php if (!empty($block['img_rep_team_home'])):
                                    $image_alt = get_post_meta($block['img_rep_team_home'], '_wp_attachment_image_alt', true); ?>
                                    <img class="team-item--image lazyload"
                                         data-src="<?php echo wp_get_attachment_image_url($block['img_rep_team_home'], 'full'); ?>"
                                         width="100" height="100" alt="<?php echo $image_alt; ?>">
                                <?php endif; ?>
                            </div>
                            <div class="media-body">
                                <h4 class="team-item--title"><?php echo $block['name_url_rep_team_home']; ?></h4>
                                <p class="team-item--job-title">
                                    <strong><?php echo $block['job_rep_team_home']; ?></strong></p>
                            </div>
                        </div>
                        <p class="team-item--description"><?php echo $block['desc_rep_team_home']; ?></p>

                        <footer _ngcontent-c5="" class="advisor-item--company">

                            <?php if (!empty($block['logo_rep_team_home'])):
                                $image_alt = get_post_meta($block['logo_rep_team_home'], '_wp_attachment_image_alt', true); ?>
                                <div _ngcontent-c5="" class="advisor-item--logos">
                                    <img _ngcontent-c5="" class="img-responsive"
                                         src="<?php echo wp_get_attachment_image_url($block['logo_rep_team_home'], 'full'); ?>"
                                         alt="<?php echo $image_alt; ?>">
                                </div>
                            <?php endif; ?>

                            <?php if (!empty($block['yt_rep_team_home'])): ?>
                                <a target="_blank" video-url="<?php echo $block['yt_rep_team_home']; ?>" _ngcontent-c5=""
                                   class="default-btn advisor-item--video open-popup-video">
                                    <div _ngcontent-c5="" class="view-video" id="video-keithTeare">
                                        <i _ngcontent-c5="" class="fa fa-caret-right"></i>
                                        <span _ngcontent-c5=""><?php echo __('View video', 'market'); ?></span>
                                    </div>
                                </a>
                            <?php endif; ?>

                        </footer>
                    </div>
                </li>

                <?php endif;
                ++$count;
            endforeach; ?>

        </ul>

        <div class="text-center">
            <a href="<?php echo carbon_get_post_meta($post->ID, 'url_btn_team_home'); ?>" class="btn default-btn btn-lg btn-social-media px-4" target="_blank">
                <span class="btn-text"><?php echo carbon_get_post_meta($post->ID, 'text_btn_team_home'); ?></span>
            </a>
        </div>

    </div>
</section>
