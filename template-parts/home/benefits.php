<!-- Section: Benefits -->
<section class="section-benefits py-5">
    <div class="container">

        <?php
        $count = 1;
        $blocks = carbon_get_post_meta($post->ID, 'rep_benefits_home');
        ?>

        <?php foreach ((array)$blocks as $block): ?>

        <?php if (!empty($block['img_rep_benefits_home']) ||
        !empty($block['title_rep_benefits_home']) ||
        !empty($block['desc_rep_benefits_home']) ||
        !empty($block['url_rep_benefits_home']) ||
        !empty($block['text_url_rep_benefits_home'])): ?>

        <!-- Benefit #<?php echo $count; ?> -->
        <?php if ($count % 2 != 0): ?>
        <div class="benefits-item row flex-column align-items-center flex-sm-row">
            <?php else: ?>
            <div class="benefits-item row flex-column align-items-center flex-sm-row-reverse">
                <?php endif; ?>
                <div class="col-md-6 text-center">
                    <?php if (!empty($block['img_rep_benefits_home'])):
                        $image_alt = get_post_meta($block['img_rep_benefits_home'], '_wp_attachment_image_alt', true); ?>
                        <div class="benefits-item--image">
                            <img class="lazyload"
                                 data-src="<?php echo wp_get_attachment_image_url($block['img_rep_benefits_home'], 'full'); ?>"
                                 width="456" height="472" alt="<?php echo $image_alt; ?>" class="img-fluid">
                        </div>
                        <!-- /.benefits-item-image -->
                    <?php endif; ?>
                </div>
                <div class="col-md-6">
                    <h2 class="benefits-item--title text-social-media"><?php echo $block['title_rep_benefits_home']; ?></h2>
                    <p class="benefits-item--desc"><?php echo $block['desc_rep_benefits_home']; ?></p>
                    <?php if (!empty($block['url_rep_benefits_home'])): ?>
                        <div>
                            <a href="<?php echo $block['url_rep_benefits_home']; ?>" class="btn default-btn btn-social-media">
                                <span><?php echo $block['text_url_rep_benefits_home']; ?></span>
                            </a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php endif; ?>

            <?php ++$count; ?>
            <?php endforeach; ?>

        </div>
</section>