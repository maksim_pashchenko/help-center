<!-- Section: Exchange -->
<section class="section-exchange py-5" style="background: #f5f7fd url(<?php echo wp_get_attachment_image_url( carbon_get_post_meta( $post->ID, 'bg_exchange_home' ), 'full' ); ?>) center center no-repeat;">
    <div class="section-exchange--inner d-flex justify-content-center align-items-center flex-wrap">
      <div class="px-4 py-3  text-center">
        <h2 class="section-title text-uppercase text-social-media font-weight-normal"><?php echo carbon_get_post_meta( $post->ID, 'title_exchange_home' ); ?></h2>
      </div>
    </div>
</section>