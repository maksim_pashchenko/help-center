<!-- Section: Books -->
<?php $blocks = carbon_get_post_meta( $post->ID, 'rep_books_home' );  ?>
<div class="footer-bg">
<section class="section-books py-5">
    <div class="container">
      <div class="row">
        <div class="col-xl-6">
          <h2 class="text-center text-md-left mb-5 text-social-media font-weight-normal"><?php echo carbon_get_post_meta( $post->ID, 'title_books_home' ); ?></h2>
        </div>
      </div>
      <div class="row">

        <?php $args = array(
            'posts_per_page' => -1,
            'post_type' => 'documents',
            'post_status' => 'publish',
            // 'suppress_filters'  => false
        );

        $query = new WP_Query($args); 

        if ($query->have_posts()) : 

          while ($query->have_posts()) : $query->the_post();

            $thumbnail_id = get_post_thumbnail_id($post->ID);
            $thumbnail = get_the_post_thumbnail_url($post->ID, 'full'); ?>

            <div class="col-12 col-sm-12 offset-sm-0 col-md-6 col-lg-3">
              <div class="book-item bg-white p-3 mb-5 mb-lg-0" data-mh="book-item">
                <div class="media">
                  <div class="media-body">
                    <h5 class="book-item--title font-weight-normal mb-3"><?php the_title(); ?></h5>
                    <div class="row no-gutters book-item--language">

                      <?php 
                      global $sitepress;
                      $trid = $sitepress->get_element_trid($post->ID);
                      $translations = $sitepress->get_element_translations($trid); 

                      foreach ((array)$translations as $key => $item):

                        $link_file = wp_get_attachment_url(carbon_get_post_meta($item->element_id, 'link_docs')); 

                        if (!empty($link_file)): ?>

                          <div class="col-6 col-sm-6 col-lg-12 col-xl-6 px-1">
                            <a href="<?php echo $link_file; ?>" class="btn btn-sm btn-block mb-2" role="button" aria-pressed="true" target="_blank">
                              <?php echo $key; ?>
                            </a>
                          </div>

                        <?php endif; 

                      endforeach; ?>

                    </div>
                  </div>
                  <div class="book-item--image">

                    <?php if (has_post_thumbnail()):
                      $image_alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true); ?>

                      <img class="lazyload" data-src="<?php echo $thumbnail; ?>" width="127" height="180" alt="<?php echo $image_alt; ?>">

                    <?php endif; ?>
                    
                  </div>
                </div>
              </div>
            </div>

          <?php endwhile;
          wp_reset_postdata(); 
        endif; ?>

      </div>
    </div>
</section>