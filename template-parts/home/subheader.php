<!-- Section: Subheader-->
<section class="section-subheader">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-4 text-center text-lg-left mb-4 mb-lg-0">

			<?php if(!empty(carbon_get_theme_option('logo_header'))): ?>

	          <a href="<?php echo esc_url(home_url('/')); ?>" class="logo-header" title="<?php echo carbon_get_theme_option('text_logo_header'); ?>" target="_blank" style="background: transparent url(<?php echo wp_get_attachment_image_url( carbon_get_theme_option('logo_header'), 'full' ); ?>) top center no-repeat;">
	            <span class="sr-only"><?php echo carbon_get_theme_option('text_logo_header'); ?></span>
	          </a>

          	<?php endif; ?>

        </div>
        <div class="col text-center text-lg-right">
          <ul class="social-links list-inline mb-4 mb-lg-0">
			<?php $icons = carbon_get_theme_option('rep_social_home'); ?>

			<?php foreach($icons as $icon): ?>

				<?php if ($icon['url_rep_social_home'] && $icon['icons_rep_social_home']['class']):  ?>

		            <li class="list-inline-item">
		              <a target="_blank" href="<?php echo $icon['url_rep_social_home']; ?>">
		                <i class="<?php echo $icon['icons_rep_social_home']['class']; ?>" aria-hidden="true"></i>
		              </a>
		            </li>

	            <?php endif; ?>
			
			<?php endforeach; ?>

          </ul>
        </div>
        <div class="col-lg-2 text-center text-lg-right">

			<?php echo market_switcher_languages('header'); ?>

        </div>
      </div>
    </div>
</section>