<!-- Section: Trusted -->
<?php $gallery = carbon_get_post_meta( $post->ID, 'gallery_trusted_home' ); ?>
<section class="section-trusted py-5">
    <div class="container">
      <div class="row text-center justify-content-center align-items-center flex-wrap">

      	<?php foreach((array)$gallery as $photo): ?>
			
			    <?php if(wp_get_attachment_image_url( $photo, 'full' )): 
            $image_alt = get_post_meta( $photo, '_wp_attachment_image_alt', true); ?>

	        	<div class="col-6 col-sm-3 col-md-2 py-2">
	        		<img class="img-fluid lazyload" data-src="<?php echo wp_get_attachment_image_url( $photo, 'full' ); ?>" alt="<?php echo $image_alt; ?>">
	        	</div>

        	<?php endif; ?>

		    <?php endforeach; ?>

      </div>
      <hr>
      <div class="text-center">
        <p class="trusted-label font-weight-bold"><?php echo carbon_get_post_meta( $post->ID, 'title_trusted_home' ); ?></p>
      </div>
    </div>
</section>