
<?php if(!empty(carbon_get_post_meta($post->ID, 'bg_img_careers'))): ?>
    <div class="careers-main" style="background: url(<?php echo carbon_get_post_meta($post->ID, 'bg_img_careers'); ?>);">
<?php else: ?>
    <div class="careers-main">
<?php endif; ?>

        <div class="container">
            <div class="career-text-item">
                <h1 class="description-title"><?php echo carbon_get_post_meta($post->ID, 'title_careers'); ?></h1>
                <p class="careers-text"><?php echo carbon_get_post_meta($post->ID, 'desc_careers'); ?></p>

                <p class="info-text"><?php echo carbon_get_post_meta($post->ID, 'info_careers'); ?></p>
            </div>
            <!-- /.careers-main-item -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.careers-main -->