<div class="description-work" data-description="<?php echo $post->ID; ?>">
    <h3><?php the_title(); ?></h3>

    <?php $tax_cities = get_the_terms($post->ID, 'vacancy_city');

    foreach ((array)$tax_cities as $tax_city) { ?>

        <span class="location-work icon-location"><?php echo $tax_city->name; ?></span>

    <?php } ?>

    <p class="invitation-work"><?php echo carbon_get_post_meta($post->ID, 'subtitle_single_careers'); ?></p>

    <div class="work-description-group">
        <?php the_content(); ?>
    </div>
    <div class="careers-form">
        <div class="careers-form-titles">
            <h3><?php echo carbon_get_post_meta( $post->ID, 'title_form_single_careers' ); ?></h3>
            <p class="privacy-policy">
                <?php echo carbon_get_post_meta( $post->ID, 'text_pp_single_careers' ); 

                if( carbon_get_post_meta( $post->ID, 'link_single_careers' ) ) { ?>
                    <a href="<?php echo carbon_get_post_meta( $post->ID, 'link_single_careers' ); ?>" target="_blank"> 
                        <?php echo carbon_get_post_meta( $post->ID, 'text_link_single_careers' ); ?> 
                    </a>
                <?php } ?>
            </p>
        </div>
    </div>
</div>