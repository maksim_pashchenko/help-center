
<?php if(!empty(carbon_get_post_meta($post->ID, 'bg_img_offer'))): ?>
    <div class="ready-offer" style="background: url(<?php echo carbon_get_post_meta($post->ID, 'bg_img_offer'); ?>); background-position: 64% 84%;">
<?php else: ?>
    <div class="ready-offer">
<?php endif; ?>

    <div class="container">
        <h2><?php echo carbon_get_post_meta($post->ID, 'title_offer'); ?></h2>

        <?php $offers = carbon_get_post_meta($post->ID, 'rep_offer'); ?>

        <?php if(!empty($offers)): ?>

            <ul class="ready-offer-list">

                <?php foreach((array)$offers as $offer): ?>

                    <?php if(!empty($offer['title_rep_offer'])): ?>

                        <li><?php echo $offer['title_rep_offer']; ?></li>

                    <?php endif; ?>

                <?php endforeach; ?>
               
            </ul>

        <?php endif; ?>

    </div>
    <!-- /.container -->
</div>
<!-- /.ready-offer -->