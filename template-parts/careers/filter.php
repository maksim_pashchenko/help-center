<?php
// Array taxonomy contries
$countries = get_terms( array(
    'taxonomy'   => array( 'vacancy_country' ),
    'hide_empty' => true
) );

// Array taxonomy cities
$cities = get_terms( array(
    'taxonomy'   => array( 'vacancy_city' ),
    'hide_empty' => true
) );

// Loop post type vacancy
$args = array(
    'posts_per_page'    => -1,
    'post_type'         => 'vacancy',
    'post_status'       => 'publish',
    'suppress_filters'  => false
);

$query = new WP_Query($args);

// Add careers form
if ( !empty(carbon_get_post_meta( $post->ID, 'shortcode_careers' )) ):

    echo do_shortcode( carbon_get_post_meta( $post->ID, 'shortcode_careers' ) );
endif; ?>

<div class="search-work">
    <div class="container">
        <div class="open-positions-item">
            <h2>Open positions</h2>
            <div class="select-location">

                <div class="select-country select-container">
                    <select class='select2' id="country">
                        <option value="1" selected disabled><?php echo __('Choose the country', 'market'); ?></option>

                        <?php foreach((array)$countries as $country): ?>

                            <option value="<?php echo $country->slug; ?>"><?php echo $country->name ?></option>

                        <?php endforeach; ?>

                    </select>
                </div>

                <div class="select-city select-container" style="display: none;">
                    <select class='select2' id="city">
                        <option value="1" selected disabled><?php echo __('Select city', 'market'); ?></option>

                        <?php foreach((array)$cities as $city):

                            $city_to_country = carbon_get_term_meta( $city->term_id, 'contry_vacancy_city' ); ?>

                            <option data-city="<?php echo $city->slug; ?>" value="<?php echo $city_to_country; ?>">
                                <?php echo $city->name ?>
                            </option>

                        <?php endforeach; ?>

                    </select>
                </div>

            </div>
        </div>

        <?php if ($query->have_posts()) { ?>

            <ul class="open-vacancies">

                <?php while ($query->have_posts()) { $query->the_post();

                    $tax_countries = get_the_terms( $post->ID, 'vacancy_country' );
                    $tax_cities    = get_the_terms( $post->ID, 'vacancy_city' );

                    $vacancy_class = '';

                    foreach ((array)$tax_countries as $tax_country) {
                        $vacancy_class .= $tax_country->slug . ' ';
                    }
                    foreach ((array)$tax_cities as $tax_city) {
                        $vacancy_class .= $tax_city->slug . ' ';
                    }

                    if(!empty($tax_countries) && !empty($tax_cities)) { ?>

                        <li class="<?php echo $vacancy_class; ?> open-vacancies--list-item" data-description="<?php echo $post->ID; ?>">
                            <span class="vacancy-title"><?php the_title(); ?></span>
                            <?php get_template_part('template-parts/careers/description', 'vacancies'); ?>
                        </li>

                    <?php }
                } wp_reset_postdata();?>

            </ul>

        <?php } ?>

    </div>

</div>