<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Market
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <title><?php wp_title("", true); ?></title>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="The first decentralized platform connecting social media bloggers and advertisers. Winner of the ICOBOX contest. Top rated ICO by trackers.">

  <!-- Favicon -->
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-16x16.png">
  <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/favicon/manifest.json">
  <link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/favicon/safari-pinned-tab.svg" color="#b31f7a">
  <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon.ico">
  <meta name="msapplication-config" content="<?php echo get_template_directory_uri(); ?>/favicon/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-touch-fullscreen" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="default">
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

  <main class="page-wrapper">

    <header class="section-header">
      <div class="container">
        <div class="row">
          <div class="col text-center py-2">
            <ul class="list-inline m-0">
              <li class="list-inline-item mr-0 mr-sm-5">
                <span class="mr-2">TOKEN PRICE</span>
                <strong id="tp" class="text-white"></strong>
                &nbsp;
                <sup id="tpp"></sup>
              </li>
              <li class="list-inline-item mr-0 mr-sm-5">
                <span class="mr-2">ETH</span>
                <strong id="eth" class="text-white"></strong>
              </li>
              <li class="list-inline-item">
                <span class="mr-2">BTC</span>
                <strong id="btc" class="text-white"></strong>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </header>