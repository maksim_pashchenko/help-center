<?php
/**
 * Template name: Careers
 */

get_header('white'); 

	global $parent_page_id;
    $parent_page_id = get_the_ID();

    get_template_part('template-parts/careers/main');

    get_template_part('template-parts/careers/offer');

    get_template_part('template-parts/careers/filter');

get_footer('white');