<?php 

/**
 * Registers a new post type "Article"
 */
function register_post_type_articles() {

	$labels = array(
		'name'               => _x( 'Articles', 'post type general name', 'market' ),
		'singular_name'      => _x( 'Article', 'post type singular name', 'market' ),
		'menu_name'          => _x( 'Articles', 'admin menu', 'market' ),
		'name_admin_bar'     => _x( 'Article', 'add new on admin bar', 'market' ),
		'add_new'            => _x( 'Add New', 'Article', 'market' ),
		'add_new_item'       => __( 'Add New Article', 'market' ),
		'new_item'           => __( 'New Article', 'market' ),
		'edit_item'          => __( 'Edit Article', 'market' ),
		'view_item'          => __( 'View Article', 'market' ),
		'all_items'          => __( 'All Articles', 'market' ),
		'search_items'       => __( 'Search Articles', 'market' ),
		'parent_item_colon'  => __( 'Parent Articles:', 'market' ),
		'not_found'          => __( 'No Articles found.', 'market' ),
		'not_found_in_trash' => __( 'No Articles found in Trash.', 'market' )
	);

	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => 'description',
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-book-alt',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'show_in_rest'		  => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'post',
		'supports'            => array(
			'title', 
			'thumbnail',
			'editor'
			)
	);

	register_post_type( 'articles', $args );
}

add_action( 'init', 'register_post_type_articles' );