<?php 

/**
 * Registers a new post type "Video"
 */
function register_post_type_announcement() {

    $labels = array(
        'name'               => _x( 'Announcements', 'post type general name', 'market' ),
        'singular_name'      => _x( 'Announcement', 'post type singular name', 'market' ),
        'menu_name'          => _x( 'Announcements', 'admin menu', 'market' ),
        'name_admin_bar'     => _x( 'Announcement', 'add new on admin bar', 'market' ),
        'add_new'            => _x( 'Add New', 'Announcement', 'market' ),
        'add_new_item'       => __( 'Add New Announcement', 'market' ),
        'new_item'           => __( 'New Announcement', 'market' ),
        'edit_item'          => __( 'Edit Announcement', 'market' ),
        'view_item'          => __( 'View Announcement', 'market' ),
        'all_items'          => __( 'All Announcements', 'market' ),
        'search_items'       => __( 'Search Announcements', 'market' ),
        'parent_item_colon'  => __( 'Parent Announcements:', 'market' ),
        'not_found'          => __( 'No Announcement found.', 'market' ),
        'not_found_in_trash' => __( 'No Announcement found in Trash.', 'market' )
    );

    $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'description'         => 'description',
        'taxonomies'          => array(),
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => null,
        'menu_icon'           => 'dashicons-format-aside',
        'show_in_nav_menus'   => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => false,
        'has_archive'         => true,
        'show_in_rest'        => true,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => true,
        'capability_type'     => 'post',
        'supports'            => array( 'title',  'editor', 'author' )
    );

    register_post_type( 'announcement', $args );
}

add_action( 'init', 'register_post_type_announcement' );

// /**
//  * Create a taxonomy "Country"
//  */
// function vacancy_country_taxonomy() {

//     $labels = array(
//         'name'                  => _x( 'Countries', 'Taxonomy Countries', 'market' ),
//         'singular_name'         => _x( 'Country', 'Taxonomy Country', 'market' ),
//         'search_items'          => __( 'Search Countries', 'market' ),
//         'popular_items'         => __( 'Popular Countries', 'market' ),
//         'all_items'             => __( 'All Countries', 'market' ),
//         'parent_item'           => __( 'Parent Country', 'market' ),
//         'parent_item_colon'     => __( 'Parent Country', 'market' ),
//         'edit_item'             => __( 'Edit Country', 'market' ),
//         'update_item'           => __( 'Update Country', 'market' ),
//         'add_new_item'          => __( 'Add New Country', 'market' ),
//         'new_item_name'         => __( 'Name New Country', 'market' ),
//         'add_or_remove_items'   => __( 'Add or Remove Country', 'market' ),
//         'choose_from_most_used' => __( 'Select a frequently used Country', 'market' ),
//         'menu_name'             => __( 'Countries', 'market' ),
//     );

//     $args = array(
//         'labels'            => $labels,
//         'public'            => true,
//         'show_in_nav_menus' => true,
//         'show_admin_column' => false,
//         'hierarchical'      => true,
//         'show_tagcloud'     => true,
//         'show_ui'           => true,
//         'query_var'         => true,
//         'rewrite'           => true,
//         'query_var'         => true,
//         'capabilities'      => array(),
//     );

//     register_taxonomy( 'vacancy_country', array( 'vacancy' ), $args );
// }

// add_action( 'init', 'vacancy_country_taxonomy' );

// /**
//  * Create a taxonomy "City"
//  */
// function vacancy_city_taxonomy() {

//     $labels = array(
//         'name'                  => _x( 'Cities', 'Taxonomy Cities', 'market' ),
//         'singular_name'         => _x( 'City', 'Taxonomy City', 'market' ),
//         'search_items'          => __( 'Search Cities', 'market' ),
//         'popular_items'         => __( 'Popular Cities', 'market' ),
//         'all_items'             => __( 'All Cities', 'market' ),
//         'parent_item'           => __( 'Parent City', 'market' ),
//         'parent_item_colon'     => __( 'Parent City', 'market' ),
//         'edit_item'             => __( 'Edit City', 'market' ),
//         'update_item'           => __( 'Update City', 'market' ),
//         'add_new_item'          => __( 'Add New City', 'market' ),
//         'new_item_name'         => __( 'Name New City', 'market' ),
//         'add_or_remove_items'   => __( 'Add or Remove City', 'market' ),
//         'choose_from_most_used' => __( 'Select a frequently used City', 'market' ),
//         'menu_name'             => __( 'Cities', 'market' ),
//     );

//     $args = array(
//         'labels'            => $labels,
//         'public'            => true,
//         'show_in_nav_menus' => true,
//         'show_admin_column' => false,
//         'hierarchical'      => true,
//         'show_tagcloud'     => true,
//         'show_ui'           => true,
//         'query_var'         => true,
//         'rewrite'           => true,
//         'query_var'         => true,
//         'capabilities'      => array(),
//     );

//     register_taxonomy( 'vacancy_city', array( 'vacancy' ), $args );
// }

// add_action( 'init', 'vacancy_city_taxonomy' );