<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make('post_meta', 'Custom Data')
	->show_on_template('template-report-bug.php')
	->show_on_post_type('page') 
    ->add_fields( array(
        Field::make( "text", "subtitle_report_bug", "Subtitle")->set_width( 100 ),
        Field::make( "text", "form_report_bug", "Short code form")->set_width( 100 )
	));