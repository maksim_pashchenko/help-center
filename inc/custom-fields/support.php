<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make('post_meta', 'Custom Data')
	->show_on_template('template-support.php')
	->show_on_post_type('page') 
    ->add_fields( array(
        Field::make( "text", "form_support", "Short code form")->set_width( 100 ),
        Field::make( "text", "subtitle_support", "Subtitle")->set_width( 100 ),
        Field::make( "text", "text_btn_support", "Text link")->set_width( 50 ),
        Field::make( 'association', 'url_btn_support' )->set_max( 1 )->set_width( 50 )
            ->set_types( array(
                array(
                    'type' => 'post',
                    'post_type' => 'page',
                )
            ) )
	));