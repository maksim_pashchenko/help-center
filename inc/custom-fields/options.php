<?php
use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'theme_options', __( 'Theme Options', 'tenthouse' ) )
    ->set_icon( 'dashicons-hammer' )
    ->set_page_menu_title( 'Setting Template' )
	->add_tab( __('Header'), array(
        Field::make( 'image', 'logo_header', 'Logo' )->set_width( 8 ),
        Field::make( 'text', 'text_logo_header', 'Text logo' )->set_width( 85 ),
        Field::make( 'complex', 'rep_social_home', 'Social' )->set_collapsed( true )
            ->add_fields( array(
                Field::make( 'icon', 'icons_rep_social_home', __( 'Social icons' ) )->set_width( 50 ),
                Field::make( "text", "url_rep_social_home", "Link")->set_width( 50 )->set_default_value( 'https://www.google.ru/' )
            )),
        Field::make( 'image', 'bg_header_search', 'Background photo (Searche page)' )->set_value_type( 'url' )
    ))
    ->add_tab( __('Blog slider'), array(
        Field::make( 'image', 'bg_slider_blog', 'Background slider' )
            ->set_value_type( 'url' )->set_default_value(get_template_directory_uri() . '/img/default-bg-slider.jpg'),
        Field::make( 'association', 'items_slider_blog' . crb_get_i18n_suffix(), 'Select articles' )
            ->set_types( array(
                array(
                    'type' => 'post',
                    'post_type' => 'post',
                )
            ) )
    ))
    /*->add_tab( __('Category slider'), array(
        Field::make( 'image', 'bg_slider_category', 'Background slider' )
            ->set_value_type( 'url' )->set_default_value(get_template_directory_uri() . '/img/default-bg-slider.jpg'),
        Field::make( 'association', 'items_slider_category', 'Select articles' )
            ->set_types( array(
                array(
                    'type' => 'post',
                    'post_type' => 'post',
                )
            ) )
    ))
    ->add_tab( __('Tag slider'), array(
        Field::make( 'image', 'bg_slider_tag', 'Background slider' )
            ->set_value_type( 'url' )->set_default_value(get_template_directory_uri() . '/img/default-bg-slider.jpg'),
        Field::make( 'association', 'items_slider_tag', 'Select articles' )
            ->set_types( array(
                array(
                    'type' => 'post',
                    'post_type' => 'post',
                )
            ) )
    ))*/
    ->add_tab( __('Social block'), array(
        Field::make( "text", "title_1_soc_block" . crb_get_i18n_suffix(), "Title left")->set_width( 50 ),
        Field::make( "text", "title_2_soc_block" . crb_get_i18n_suffix(), "Title right")->set_width( 50 ),
        Field::make( "text", "telegramm_soc_block" . crb_get_i18n_suffix(), "Link Telegramm")->set_width( 50 ),
        Field::make( 'complex', 'rep_social_soc_block', 'Social' )->set_collapsed( true )->set_width( 50 )
            ->add_fields( array(
                Field::make( 'icon', 'icons_rep_social_soc_block', __( 'Social icons' ) )->set_width( 50 ),
                Field::make( "text", "url_rep_social_soc_block", "Link")->set_width( 50 )->set_default_value( 'https://www.google.ru/' )
            ))
    ))
    ->add_tab( __('FaQ search'), array(
        Field::make( "text", "title_search_faq" . crb_get_i18n_suffix(), "Title"),
        Field::make( 'association', 'items_search_faq' . crb_get_i18n_suffix(), 'Select question (FaQ)' )
            ->set_types( array(
                array(
                    'type' => 'post',
                    'post_type' => 'faq',
                )
            ) )
    ))
    ->add_tab( __('Footer'), array(
        Field::make( 'text', 'title_menu_1_footer' . crb_get_i18n_suffix(), 'Заглавие меню 1' )->set_width( 20 ),
        Field::make( 'text', 'title_menu_2_footer' . crb_get_i18n_suffix(), 'Заглавие меню 2' )->set_width( 20 ),
        Field::make( 'text', 'title_menu_3_footer' . crb_get_i18n_suffix(), 'Заглавие меню 3' )->set_width( 20 ),
        Field::make( 'text', 'title_menu_4_footer' . crb_get_i18n_suffix(), 'Заглавие меню 4' )->set_width( 20 ),
        Field::make( 'complex', 'rep_social_footer', 'Social' )->set_collapsed( true )
            ->add_fields( array(
                Field::make( 'icon', 'icons_rep_social_footer', __( 'Social icons' ) )->set_width( 50 ),
                Field::make( "text", "url_rep_social_footer", "Link")->set_width( 50 )->set_default_value( 'https://www.google.ru/' )
            )),
        Field::make( 'image', 'logo_footer', 'Logo' )->set_width( 8 ),
        Field::make( 'text', 'text_logo_footer' . crb_get_i18n_suffix(), 'Text logo' )->set_width( 85 ),
        Field::make( 'text', 'copyright_footer' . crb_get_i18n_suffix(), 'Copyright' )->set_width( 70 ),
        Field::make( 'text', 'mail_footer' . crb_get_i18n_suffix(), 'Mail' )->set_width( 30 ),
        Field::make( 'text', 'text_polici_footer' . crb_get_i18n_suffix(), 'Text link 1' )->set_width( 50 ),
        Field::make( 'text', 'polici_footer' . crb_get_i18n_suffix(), 'Link 1' )->set_width( 50 ),
        Field::make( 'text', 'text_terms_footer' . crb_get_i18n_suffix(), 'Text link' )->set_width( 50 ),
        Field::make( 'text', 'terms_footer' . crb_get_i18n_suffix(), 'Link2' )->set_width( 50 ),
        Field::make( 'footer_scripts', 'script_footer', 'Semantic Markup' )->set_width( 100 )
    ));