<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make('post_meta', 'Custom Data')
	->show_on_template('template-contact-us.php')
	->show_on_post_type('page') 
    ->add_fields( array(
        Field::make( "text", "form_contact_us", "Short code form")->set_width( 100 )
	));