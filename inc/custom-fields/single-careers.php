<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make('post_meta', 'Custom Data')
	->show_on_post_type('vacancy') 
    ->add_tab( 'Main', array(
        Field::make( "textarea", "subtitle_single_careers", "Subtitle")->set_width( 100 ),
        Field::make( "text", "title_form_single_careers", "Title form")->set_width( 100 ),
        Field::make( "text", "text_pp_single_careers", "Text (privacy-policy)")->set_width( 50 ),
        Field::make( "text", "text_link_single_careers", "Text link")->set_width( 50 ),
        Field::make( 'text', 'link_single_careers', 'Link (privacy-policy)' )
    ));