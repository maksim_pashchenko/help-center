<?php 

use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'term_meta', 'Category setting' )
    ->where( 'term_taxonomy', '=', 'faq_main_tax' )
    ->add_fields( array(
        Field::make( 'image', 'icon_faq_main_tax', 'Icon' ),
        Field::make( 'color', 'color_faq_main_tax', 'Color' )
    ) );