<?php 

use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'term_meta', 'Category Properties' )
    ->where( 'term_taxonomy', '=', 'vacancy_city' )
    ->add_fields( array(
        Field::make( 'select', 'contry_vacancy_city', 'Countries' )
		    ->add_options("get_countries")
		    ->set_required( true )
    ) );