<?php 

use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'term_meta', 'Category Properties' )
    ->where( 'term_taxonomy', '=', 'video_cat' )
    ->add_fields( array(
        Field::make( 'color', 'color_video_cat' )->set_default_value( '#aa4bd6' )
    ) );