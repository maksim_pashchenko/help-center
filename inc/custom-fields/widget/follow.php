<?php 

use Carbon_Fields\Widget;
use Carbon_Fields\Field;

class Follow_Widget extends Widget {
    // Register widget function. Must have the same name as the class
    function __construct() {
        $this->setup( 'theme_widget_example', 'Follow us', 'Displays a block with title/text', array(
            Field::make( "text", "title_follow" . $this->market_get_i18n_suffix(), "Social link"),
            Field::make( 'complex', 'rep_follow', 'Social' )
                ->add_fields( array(
                    Field::make( "text", "url_rep_follow", "Social link"),
                    Field::make( 'color', 'bg_rep_follow', 'Background color' ),
                    Field::make( 'icon', 'icons_rep_follow', 'Social icons' )
                ))
        ) );
    }

    // Called when rendering the widget in the front-end
    function front_end( $args, $instance ) { ?>

        <div class="blog-follow">
            <p class="widget-title"><?php echo $instance['title_follow' . $this->market_get_i18n_suffix()]; ?></p>
            <ul class="blog-follow-social">

            <?php foreach((array)$instance['rep_follow'] as $item): ?>
                <li style="background: <?php print_r( $item['bg_rep_follow'] ); ?>;">
                    <a target="_blank" href="<?php echo $item['url_rep_follow']; ?>">
                        <i class="<?php echo $item['icons_rep_follow']['class']; ?>"></i>
                    </a>
                </li>
            <?php endforeach; ?>

            </ul>
        </div>

    <?php }

    function market_get_i18n_suffix() {
        $suffix = '';
        if ( ! defined( 'ICL_LANGUAGE_CODE' ) ) {
            return $suffix;
        }
        $suffix = '_' . ICL_LANGUAGE_CODE;

        return $suffix;
    }
}

// function load_widgets() {
//     register_widget( 'Follow_Widget' );
// }