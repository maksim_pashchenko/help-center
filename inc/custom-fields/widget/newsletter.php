<?php 

use Carbon_Fields\Widget;
use Carbon_Fields\Field;

class Newsletter_Widget extends Widget {
    // Register widget function. Must have the same name as the class
    function __construct() {
        $this->setup( 'theme_widget_newsletter', 'Carbon Newsletter', 'Displays a block with title/text', array(
            Field::make( "text", "title_newsletter" . $this->market_get_i18n_suffix(), "Title"),
            Field::make( "textarea", "desc_newsletter" . $this->market_get_i18n_suffix(), "Description")
        ) );
    }

    // Called when rendering the widget in the front-end
    function front_end( $args, $instance ) { ?>

        <div class="blog-newsletter">
            <div><?php echo $instance['title_newsletter' . $this->market_get_i18n_suffix()]; ?></div>
            <p><?php echo $instance['desc_newsletter' . $this->market_get_i18n_suffix()]; ?></p>
            <form class="blog-newsletter-form" method="post" action="<?php echo home_url('/'); ?>?na=s" onsubmit="return newsletter_check(this)">
                <input type="hidden" name="nlang" value="en">
                <input type="hidden" name="nr" value="widget">
                <input type="hidden" name="nl[]" value="0">
                <input class="tnp-email" type="email" name="ne" placeholder="<?php echo __('Enter your email', 'market'); ?>" required>
                <button><img src="<?php echo get_template_directory_uri(); ?>/img/arrow-right.png" alt=">"></button>
            </form>
        </div>

    <?php }

    function market_get_i18n_suffix() {
        $suffix = '';
        if ( ! defined( 'ICL_LANGUAGE_CODE' ) ) {
            return $suffix;
        }
        $suffix = '_' . ICL_LANGUAGE_CODE;

        return $suffix;
    }
}

function load_widgets() {
    register_widget( 'Follow_Widget' );
    register_widget( 'Newsletter_Widget' );
}