<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make('post_meta', 'Custom Data')
	->show_on_post_type('articles') 
	->set_context('side')
    ->add_fields( array(
        Field::make( "file", "link_docs", "Book (PDF)")
        	// ->set_value_type( "url" )
    ));