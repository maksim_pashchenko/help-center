<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

// Container::make('post_meta', 'Custom Data')
// 	->show_on_post_type('announcement') 
//     ->add_tab( 'Main', array(
//     	Field::make( "image", "bg_img_careers", "Background image")->set_value_type( 'url' )->set_width( 8 ),
//     	Field::make( "text", "title_careers", "Title")->set_width( 20 ),
//     	Field::make( "textarea", "desc_careers", "Description")->set_width( 30 ),
//     	Field::make( "text", "info_careers", "Information")->set_width( 30 )
//     ))
//     ->add_tab( 'Offer', array(
//     	Field::make( "image", "bg_img_offer", "Background image")->set_value_type( 'url' )->set_width( 8 ),
//         Field::make( "text", "title_offer", "Title")->set_width( 80 ),
//         Field::make( 'complex', 'rep_offer', 'Offer list' )->set_collapsed( true )
//             ->add_fields( array(
//                 Field::make( "text", "title_rep_offer", "Title")
//             ))
//     ))
//     ->add_tab( 'Filter', array(
//         Field::make( "text", "shortcode_careers", "Short code form")
// 	));