<?php 

/**
 * Registers a new post type "Video"
 */
function register_post_type_video() {

    $labels = array(
        'name'               => _x( 'Videos', 'post type general name', 'market' ),
        'singular_name'      => _x( 'Video', 'post type singular name', 'market' ),
        'menu_name'          => _x( 'Videos', 'admin menu', 'market' ),
        'name_admin_bar'     => _x( 'Video', 'add new on admin bar', 'market' ),
        'add_new'            => _x( 'Add New', 'Video', 'market' ),
        'add_new_item'       => __( 'Add New Video', 'market' ),
        'new_item'           => __( 'New Video', 'market' ),
        'edit_item'          => __( 'Edit Video', 'market' ),
        'view_item'          => __( 'View Video', 'market' ),
        'all_items'          => __( 'All Videos', 'market' ),
        'search_items'       => __( 'Search Videos', 'market' ),
        'parent_item_colon'  => __( 'Parent Videos:', 'market' ),
        'not_found'          => __( 'No Video found.', 'market' ),
        'not_found_in_trash' => __( 'No Video found in Trash.', 'market' )
    );

    $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'description'         => 'description',
        'taxonomies'          => array(),
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => null,
        'menu_icon'           => 'dashicons-format-video',
        'show_in_nav_menus'   => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => false,
        'has_archive'         => true,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => true,
        'capability_type'     => 'post',
        'supports'            => array( 'title', 'author' )
    );

    register_post_type( 'video', $args );
}

add_action( 'init', 'register_post_type_video' );




/**
 * Create a taxonomy
 */
function video_taxonomy() {

    $labels = array(
        'name'                  => _x( 'Categories', 'Taxonomy Categories', 'market' ),
        'singular_name'         => _x( 'Category', 'Taxonomy Category', 'market' ),
        'search_items'          => __( 'Search Categories', 'market' ),
        'popular_items'         => __( 'Popular Categories', 'market' ),
        'all_items'             => __( 'All Categories', 'market' ),
        'parent_item'           => __( 'Parent Category', 'market' ),
        'parent_item_colon'     => __( 'Parent Category', 'market' ),
        'edit_item'             => __( 'Edit Category', 'market' ),
        'update_item'           => __( 'Update Category', 'market' ),
        'add_new_item'          => __( 'Add New Category', 'market' ),
        'new_item_name'         => __( 'Name New Category', 'market' ),
        'add_or_remove_items'   => __( 'Add or Remove Category', 'market' ),
        'choose_from_most_used' => __( 'Select a frequently used Category', 'market' ),
        'menu_name'             => __( 'Categories', 'market' ),
    );

    $args = array(
        'labels'            => $labels,
        'public'            => true,
        'show_in_nav_menus' => true,
        'show_admin_column' => false,
        'hierarchical'      => true,
        'show_tagcloud'     => true,
        'show_ui'           => true,
        'query_var'         => true,
        'rewrite'           => true,
        'query_var'         => true,
        'capabilities'      => array(),
        "rewrite" => array(     
            'with_front' => false     
        )
    );

    register_taxonomy( 'video_cat', array( 'video' ), $args );
}

add_action( 'init', 'video_taxonomy' );