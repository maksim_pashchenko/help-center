<?php 

/**
 * Registers a new post type "Article"
 */
function register_post_type_get_started() {

	$labels = array(
		'name'               => _x( 'Get Started', 'post type general name', 'market' ),
		'singular_name'      => _x( 'Get Started', 'post type singular name', 'market' ),
		'menu_name'          => _x( 'Get Started', 'admin menu', 'market' ),
		'name_admin_bar'     => _x( 'Get Started', 'add new on admin bar', 'market' ),
		'add_new'            => _x( 'Add New', 'Get Started', 'market' ),
		'add_new_item'       => __( 'Add New Get Started', 'market' ),
		'new_item'           => __( 'New Get Started', 'market' ),
		'edit_item'          => __( 'Edit Get Started', 'market' ),
		'view_item'          => __( 'View Get Started', 'market' ),
		'all_items'          => __( 'All Get Started', 'market' ),
		'search_items'       => __( 'Search Get Started', 'market' ),
		'parent_item_colon'  => __( 'Parent Get Started:', 'market' ),
		'not_found'          => __( 'No Get Started found.', 'market' ),
		'not_found_in_trash' => __( 'No Get Started found in Trash.', 'market' )
	);

	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => 'description',
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-book-alt',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'show_in_rest'        => true,
		'capability_type'     => 'post',
		'supports'            => array(
			'title', 
			'thumbnail',
			'editor'
			)
	);

	register_post_type( 'get_started', $args );
}

add_action( 'init', 'register_post_type_get_started' );