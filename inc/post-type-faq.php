<?php 

/**
 * Registers a new post type "FAQ"
 */
function register_post_type_faq() {

	$labels = array(
		'name'               => _x( 'FAQ', 'post type general name', 'market' ),
		'singular_name'      => _x( 'FAQ', 'post type singular name', 'market' ),
		'menu_name'          => _x( 'FAQ', 'admin menu', 'market' ),
		'name_admin_bar'     => _x( 'FAQ', 'add new on admin bar', 'market' ),
		'add_new'            => _x( 'Add New', 'FAQ', 'market' ),
		'add_new_item'       => __( 'Add New FAQ', 'market' ),
		'new_item'           => __( 'New FAQ', 'market' ),
		'edit_item'          => __( 'Edit FAQ', 'market' ),
		'view_item'          => __( 'View FAQ', 'market' ),
		'all_items'          => __( 'All FAQ', 'market' ),
		'search_items'       => __( 'Search FAQ', 'market' ),
		'parent_item_colon'  => __( 'Parent FAQ:', 'market' ),
		'not_found'          => __( 'No FAQ found.', 'market' ),
		'not_found_in_trash' => __( 'No FAQ found in Trash.', 'market' )
	);

	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => 'description',
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-editor-help',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
        'rewrite'             => array(
            'slug' => 'faq/%faq_main_tax%',
            'with_front' => false,
        ),
		'capability_type'     => 'post',
		'supports'            => array(
			'title', 
            'editor',
			'thumbnail'
			)
	);

	register_post_type( 'faq', $args );
}

add_action( 'init', 'register_post_type_faq' );


add_action( 'init', 'create_faq_main_tax' );
add_action( 'init', 'create_faq_tax' );

function create_faq_main_tax() {
    register_taxonomy(
        'faq_main_tax',
        'faq',
        array(
            'label' => __( 'Main category' ),
            'rewrite' => array(
                'slug' => 'faq',
                'with_front' => false,
            ),
            'hierarchical' => true,
        )
    );
}

function create_faq_tax() {
    register_taxonomy(
        'faq_tax',
        'faq',
        array(
            'label' => __( 'Category' ),
            'rewrite' => array( 'slug' => 'faq_tax' ),
            'hierarchical' => true,
        )
    );
}

use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make('post_meta', 'Custom Data')
    ->show_on_post_type('faq')
    ->add_fields( array(
        Field::make( 'oembed', 'faq_video_oembed' ),
        Field::make( 'text', 'faq_positive_response' ),
    ));


Container::make('post_meta', 'faq_data',__('FAQ data', 'smm'))
    ->where('post_template', '=', 'template-faq.php')
    ->add_fields([
        Field::make( 'association', 'faq_association' )
            ->set_types( array(
                array(
                    'type' => 'post',
                    'post_type' => 'faq',
                ),
            ) ),

        Field::make( 'association', 'video_association' )
            ->set_types( array(
                array(
                    'type' => 'post',
                    'post_type' => 'video',
                ),
            ) ),

        Field::make( 'text', 'text_link_faq', 'Text button' )->set_width( 50 ),
        Field::make( 'text', 'link_faq', 'Link button' )->set_width( 50 )
    ]);

function faq_rewrite_rules( $rules ) {
    $new = array();
    $new['faq/([^/]+)/(.+)/?$'] = 'index.php?faq=$matches[2]';
    $new['faq/(.+)/?$'] = 'index.php?faq_main_tax=$matches[1]';

    return array_merge( $new, $rules ); // Ensure our rules come first
}
add_filter( 'rewrite_rules_array', 'faq_rewrite_rules' );

function faq_filter_post_type_link( $link, $post ) {
    if ( $post->post_type == 'faq' ) {
        if ( $cats = get_the_terms( $post->ID, 'faq_main_tax' ) ) {
            $link = str_replace( '%faq_main_tax%', current( $cats )->slug, $link );
        }
    }
    return $link;
}
add_filter( 'post_type_link', 'faq_filter_post_type_link', 10, 2 );


