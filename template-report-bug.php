<?php
/**
 * Template name: Report bug page
 */

get_header('white'); ?>

    <div class="page-content">
        <div class="container">
            <div class="description-page">
                <h1><?php the_title(); ?></h1> 
                <p><?php echo carbon_get_post_meta($post->ID, 'subtitle_report_bug'); ?></p>
            </div>

            <?php echo do_shortcode( carbon_get_post_meta($post->ID, 'form_report_bug') ); ?>

            <?php get_template_part('template-parts/social', 'block'); ?>

        </div>
    </div>

<?php get_footer('white');
