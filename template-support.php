<?php
/**
 * Template name: Support page
 */

get_header('white'); ?> 

    <div class="page-content">
        <div class="container">
            <div class="description-page">
                <h1><?php the_title(); ?></h1>
                <div class="description-page-info">
                   <p> <?php echo carbon_get_post_meta($post->ID, 'subtitle_support'); ?></p>

                    <?php
                    $url = carbon_get_post_meta($post->ID, 'url_btn_support'); 

                    if( isset( $url[0] ) && isset( $url[0]['id'] ) ): ?>
                        <a href="<?php echo get_permalink($url[0]['id']); ?>" class="btn btn-gradient-purple">
                            <?php echo carbon_get_post_meta($post->ID, 'text_btn_support'); ?>
                        </a>
                    <?php endif; ?>
                </div>
                <!-- /.description-page-btn -->
            </div>

            <?php echo do_shortcode( carbon_get_post_meta($post->ID, 'form_support') ); ?>

            <?php get_template_part('template-parts/social', 'block'); ?>

        </div>
        <!-- /.container -->
    </div>
    <!-- /.page-content -->

<?php get_footer('white');