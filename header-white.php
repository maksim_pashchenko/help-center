<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Market
 */
?>

<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/favicon/manifest.json">
        <link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/favicon/safari-pinned-tab.svg" color="#b31f7a">
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon.ico">
        <meta name="msapplication-config" content="<?php echo get_template_directory_uri(); ?>/favicon/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="default">

        <?php wp_head(); ?>
    </head>

<body <?php body_class(); ?>>

    <?php if (!is_page_template('template-home.php')) { ?>

        <div class="mobile-menu">
            <div class="mnu">

                <?php if (has_nav_menu('header_2')) {
                    wp_nav_menu(array(
                        'theme_location' => 'header_2',
                        'container' => false,
                        'items_wrap' => '<nav class="header-nav">%3$s</nav>',
                    ));
                } ?>
                <?php //echo market_switcher_languages('header'); ?>

                <div class="swiper-language swiper-container">
                    <div class="swiper-wrapper">
                        <?php $languages = icl_get_languages('skip_missing=1'); ?>
                        <?php foreach ($languages as $language) : ?>

                            <div class="swiper-slide" data-language="<?php echo $language['code'] ?>">
                                <a href="<?php echo $language['url'] ?>"><span><?php echo $language['native_name'] ?></span></a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <!-- /.swiper-wrapper -->
                    <div class="custom-btn custom-btn-prev"></div>
                    <div class="custom-btn custom-btn-next"></div>
                </div>
                <!-- /.swiper-container -->
            </div>
        </div>

    <section class="header <?php echo is_page_template('template-careers-page.php') ? 'header-careers' : ''; ?>">
        <div class="header-main-top header-main">
            <div class="container">
                <div class="toggle-nav">
                    <span></span>
                </div>

                    <div class="header-nav-wrapper">
                        <div class="logo">
                            <a href="<?php echo esc_url(home_url('/')); ?>" class="header-logo">
                                <?php $svg_file = file_get_contents(wp_get_attachment_image_url(carbon_get_theme_option('logo_header'), 'full')); ?>
                                <?php echo $svg_file; ?>
                            </a>
                        </div>

                        <div class="header-nav-container">

                            <?php if (has_nav_menu('header_1')) {
                                wp_nav_menu(array(
                                    'theme_location' => 'header_1',
                                    'container' => false,
                                    'items_wrap' => '<nav class="header-nav">%3$s</nav>',
                                ));
                            } ?>

                            <?php echo market_switcher_languages('header'); ?>

                            <?php get_template_part('template-parts/sub', 'menu'); ?>


                            </div>
                            <!-- /.header-tooltip -->

                            <!-- /.header-tooltip -->
                        </div>
                        <!-- /.header-nav-container -->
                    </div>
                </div>
        </section>
    <?php } ?>
