<?php
/**
 * Template name: FAQ
 */

get_header('white');

    $faq_page = get_pages(array(
        'meta_key' => '_wp_page_template',
        'meta_value' => 'template-faq.php'
    ))[0];

    get_template_part('template-parts/faq/search', 'form'); 

    get_template_part('template-parts/faq/question', 'list'); 

    get_template_part('template-parts/faq/popular', 'topics'); 

    get_template_part('template-parts/faq/video'); 

get_footer('white');