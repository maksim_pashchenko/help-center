<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Market
 */

get_header('white');
$blog_page = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'template-blog.php'
))[0];
?>

<div class="page-content error-page">
    <div class="container">
        <div class="description-page">
            <span class="description-title">Hey, something went wrong :(</span>

            <div class="link-another-page">
                <a href="<?php echo get_home_url(); ?>"><?php echo __('Return to Home Page', 'market'); ?></a>
                <?php if ($blog_page) : ?>
                    <span>|</span>
                    <a href="<?php echo get_the_permalink($blog_page->ID); ?>"><?php echo __('Read our interesting Blog', 'market'); ?></a>
                <?php endif; ?>
            </div>
            <!-- /.link-another-page -->
        </div>
        <div class="error-page-img"></div>
        <!-- /.construction-page-img -->
    </div>
</div>



<?php get_footer('white');