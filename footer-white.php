<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Market
 */
?>

<?php if (is_page_template(array('template-careers-page.php', 'template-single-faq.php'))): ?>


    <footer class="section-footer">
        <div class="container">
            <div class="footer-items">
                <div class="footer-item select-language">
                    <div class="dropdown-language">
                        <span class="icon-language">A</span>
                        <?php echo market_switcher_languages('footer'); ?>
                    </div>
                    <!-- /.dropdown-language -->
                </div>

                <div class="footer-item">
                    <div class="footer-title">
                        <?php if (!crb_get_i18n_theme_option('title_menu_1_footer')) {
                            echo '&nbsp;';
                        } else {
                            echo crb_get_i18n_theme_option('title_menu_1_footer');
                        } ?>
                    </div>
                    <?php if (has_nav_menu('footer_1')) {
                        wp_nav_menu(array(
                            'theme_location' => 'footer_1',
                            'container' => false,
                            'items_wrap' => '<ul class="footer-nav">%3$s</ul>',
                        ));
                    } ?>
                </div>

                <div class="footer-item">
                    <div class="footer-title">
                        <?php if (!crb_get_i18n_theme_option('title_menu_2_footer')) {
                            echo '&nbsp;';
                        } else {
                            echo crb_get_i18n_theme_option('title_menu_2_footer');
                        } ?>
                    </div>
                    <?php if (has_nav_menu('footer_2')) {
                        wp_nav_menu(array(
                            'theme_location' => 'footer_2',
                            'container' => false,
                            'items_wrap' => '<ul class="footer-nav">%3$s</ul>',
                        ));
                    } ?>
                </div>

                <div class="footer-item">
                    <div class="footer-title">
                        <?php if (!crb_get_i18n_theme_option('title_menu_3_footer')) {
                            echo '&nbsp;';
                        } else {
                            echo crb_get_i18n_theme_option('title_menu_3_footer');
                        } ?>
                    </div>
                    <?php if (has_nav_menu('footer_3')) {
                        wp_nav_menu(array(
                            'theme_location' => 'footer_3',
                            'container' => false,
                            'items_wrap' => '<ul class="footer-nav">%3$s</ul>',
                        ));
                    } ?>
                </div>

                <div class="footer-item">
                    <div class="footer-title">
                        <?php if (!crb_get_i18n_theme_option('title_menu_4_footer')) {
                            echo '&nbsp;';
                        } else {
                            echo crb_get_i18n_theme_option('title_menu_4_footer');
                        } ?>
                    </div>
                    <?php if (has_nav_menu('footer_4')) {
                        wp_nav_menu(array(
                            'theme_location' => 'footer_4',
                            'container' => false,
                            'items_wrap' => '<ul class="footer-nav">%3$s</ul>',
                        ));
                    } ?>
                </div>

                <div class="footer-item social-links">
                    <!-- Social links starts -->
                    <?php $icons = carbon_get_theme_option('rep_social_footer');
                    $count = 1; ?>
                            <?php foreach ($icons as $icon): ?>

                                <?php if ($icon['url_rep_social_footer'] && $icon['icons_rep_social_footer']['class']): ?>

                                    <div class="social-link">
                                        <a rel="nofollow" target="_blank"
                                           href="<?php echo $icon['url_rep_social_footer']; ?>">
                                            <i class="<?php echo $icon['icons_rep_social_footer']['class']; ?>"
                                               aria-hidden="true"></i>
                                        </a>
                                    </div>

                                <?php endif;
                                ++$count;
                            endforeach; ?>
                    <!-- Social links ends -->
                </div>
            </div>
            <!-- /.footer-items -->
        </div>
        <!-- /.container -->
    </footer>

<?php endif; ?>

<!-- Section: Footer Copyright-->
<section class="section-copyrights">
    <div class="container">
        <div class="section-copyrights-wrap">
            <div class="footer-logo">
                <!-- <a href="<?php //echo esc_url(home_url('/')); ?>" class="logo-footer" title="<?php //echo carbon_get_theme_option('text_logo_footer'); ?>" target="_blank" style="background: transparent url(<?php //echo wp_get_attachment_image_url( carbon_get_theme_option('logo_footer'), 'full' ); ?>) top center no-repeat;"> -->
                <a href="<?php echo esc_url(home_url('/')); ?>"
                   title="<?php echo crb_get_i18n_theme_option('text_logo_footer'); ?>" target="_blank">
                    <?php $svg_file = file_get_contents(wp_get_attachment_image_url(carbon_get_theme_option('logo_header'), 'full')); ?>
                    <?php echo $svg_file; ?>
                    <!--                    -->
                    <?php //echo carbon_get_theme_option('text_logo_footer'); ?><!--</span>-->
                </a>
            </div>
            <div class="footer-privacy">
                <p class=""><?php echo crb_get_i18n_theme_option('copyright_footer'); ?></p>
                <ul class="privacy-list">
                    <li>
                        <?php if (!empty(crb_get_i18n_theme_option('polici_footer')) && !empty(crb_get_i18n_theme_option('text_polici_footer'))): ?>
                            <a href="<?php echo crb_get_i18n_theme_option('polici_footer'); ?>" target="_blank">
                                <?php echo crb_get_i18n_theme_option('text_polici_footer'); ?>
                            </a>
                        <?php endif; ?>
                    </li>
                    <li>
                        <?php if (!empty(crb_get_i18n_theme_option('terms_footer')) && !empty(crb_get_i18n_theme_option('text_terms_footer'))): ?>
                            <a href="<?php echo crb_get_i18n_theme_option('terms_footer'); ?>" target="_blank">
                                <?php echo crb_get_i18n_theme_option('text_terms_footer'); ?>
                            </a>
                        <?php endif; ?>
                    </li>
                </ul>
            </div>
            <div class="">
                <p class="">
                    <?php if (!empty(crb_get_i18n_theme_option('mail_footer'))): ?>
                        <a href="mailto:<?php echo crb_get_i18n_theme_option('mail_footer'); ?>">
                            <?php echo crb_get_i18n_theme_option('mail_footer'); ?>
                        </a>
                    <?php endif; ?>
                </p>
            </div>
        </div>
    </div>
</section>

<?php echo carbon_get_theme_option('script_footer'); ?>

<!-- scripts -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/vendors/fotorama.js"></script>
<!-- <script src="<?php //echo get_template_directory_uri(); ?>/js/main.js"></script> -->
<?php wp_footer(); ?>
</body>
</html>