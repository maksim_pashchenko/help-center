<?php
/**
 * Template name: Construction page
 */

get_header('white');

$blog_page = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'template-blog.php'
))[0];
?>

    <div class="page-content construction-page">
        <div class="container">
            <div class="description-page">
                <h1><?php echo __('In development page', 'market'); ?></h1>

                <div class="link-another-page">
                    <a href="<?php echo get_home_url(); ?>"><?php echo __('Return to Home Page', 'market'); ?></a>
                    <?php if ($blog_page) : ?>
                        <span>|</span>
                        <a href="<?php echo get_the_permalink($blog_page->ID); ?>"><?php echo __('Read our interesting Blog', 'market'); ?></a>
                    <?php endif; ?>
                </div>
                <!-- /.link-another-page -->
            </div>
            <div class="construction-page-img"></div>
            <!-- /.construction-page-img -->
        </div>
    </div>

<?php get_footer('white');