<?php

get_header('white'); 

    get_template_part('template-parts/faq/search', 'form'); ?> 

    <section class="faq-content-section">
        <div class="container">
            <div class="faq-content-container">
                <div class="faq-content-wrapper">
                    <div class="breadcrumbs">
                        <?php get_template_part('template-parts/breadcrumbs', 'lang'); ?>
                    </div>
                    <div class="faq-content">
                        <h1 class="faq-helpful-block__title"><?php the_title(); ?></h1>
                        <?php the_content(); ?>
                    </div>
                    <div class="faq-helpful-block">
                        <h3 class="faq-helpful-block__title"><?php echo __('Was this helpful?', 'market'); ?></h3>
                        <div class="faq-helpful-block__btn-set">
                            <button class="faq-helpful-block__btn faq_yes_button icon-ok" data-id="<?php echo get_the_ID() ?>">
                                <?php echo __('Yes', 'market'); ?>
                            </button>
                            <button class="faq-helpful-block__btn icon-cancel" onclick="parent.LC_API.open_chat_window({source:'minimized'}); return false">
                                <?php echo __('No', 'market'); ?>
                            </button>
                        </div>
                    </div>

                    <div class="faq-success-block">
                        <h3 class="faq-success-block__title icon-ok">
                            <?php echo __('Thanks for the positive review', 'market'); ?>
                        </h3>
                    </div>

                    <div class="faq-support-block">
                        <h3 class="faq-support-block__title"><?php echo __('Thanks for the positive review', 'market'); ?></h3>
                        <button onclick="parent.LC_API.open_chat_window({source:'minimized'}); return false" class="faq-support-block__btn"><?php echo __('Contact manager', 'market'); ?></button>
                    </div>

                    <div class="faq-articles-block">
                        <h3 class="faq-articles-block__title"><?php echo __('Articles in this section', 'market'); ?></h3>
                        <div class="faq-articles-wrapper">
                            <ul class="faq-list">

                                <?php
                                $args = array(
                                    'post_type' => 'faq',
                                    'post__not_in' => array($post->ID),
                                    'suppress_filters' => false,
                                    'posts_per_page' => -1
                                );
                                $current_id = get_the_ID();
                                $posts = get_posts($args);
                                $faq_ids = array_reverse(array_column($posts, 'ID'));
                                $current_pos = array_search($current_id, $faq_ids);
                                $all_faqs = count($posts);
                                $offset = 0;

                                //                                if ( $all_faqs <= 10 ) {
                                //                                     $offset = 0;
                                //                                } else {
                                //                                    if ( ( $all_faqs - $current_pos ) >= 5 ) {
                                //
                                //                                    } else {
                                //                                        $offset = $all_faqs - 10;
                                //                                    }
                                //                                }
                                //
                               foreach($posts as $post) {
                                   // if( $post->ID == $current_id ) { ?>
                                        
                                        <li class="faq-item">
                                            <a href="<?php echo get_the_permalink($post->ID); ?>" class="faq-item__link">
                                                <?php echo get_the_title($post->ID); ?>

                                                <?php if ( carbon_get_post_meta( $post->ID, 'faq_video_oembed' ) ) : ?>
                                                    <i class="fas fa-video icon-videocam"></i>
                                                <?php endif; ?>
                                            </a> 
                                        </li>

                                   <?php // }
                               }

                                ?>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="sidebar-container faq-sidebar-container">
                    <div class="faq-question-list">

                        <?php
                        $main_terms = get_terms(array(
                            'taxonomy' => 'faq_main_tax',
                            'hide_empty' => true,
                        ));

                        $terms = wp_get_post_terms(get_the_ID(), 'faq_main_tax');

                        if (!is_wp_error($main_terms)) {
                            foreach ($main_terms as $term) {
                                $active = '';
                                if ($terms[0]->slug == $term->slug) {
                                    $active = 'active';
                                }
                                $color = carbon_get_term_meta( $term->term_id, 'color_faq_main_tax' ); 
                                $icon  = carbon_get_term_meta( $term->term_id, 'icon_faq_main_tax' ); ?>

                                <a id="icon_<?php echo $term->term_id; ?>" href="<?php echo get_term_link($term) ?>" class="faq-question-item <?php echo $active; ?>">

                                    <?php if ( !empty($icon) && get_post_mime_type($icon) == 'image/svg+xml' ) : 

                                        if ( !empty($color) ) :
                                            echo '<style>#icon_'.$term->term_id.' path {fill: '.$color.';}</style>';
                                        endif;

                                        $svg_file = file_get_contents(wp_get_attachment_image_url($icon, 'full'));
                                        echo $svg_file;

                                    elseif ( !empty($icon) && get_post_mime_type($icon) == 'image/png' ) : 

                                        echo '<img src="'.wp_get_attachment_image_url($icon, 'full').'" alt="icon">';

                                    endif; ?>

                                    <h3 class="faq-question-item__title"><?php echo $term->name ?></h3>
                                </a>

                            <?php }
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer('white');