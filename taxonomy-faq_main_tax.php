<?php

get_header('white');

    get_template_part('template-parts/faq/search', 'form'); ?>

    <section class="faq-content-section">
        <div class="container">
            <div class="faq-content-container">
                <div class="faq-content-wrapper faq-articles">

                    <?php $current_tax = $wp_query->get_queried_object(); ?>

                    <h3 class="faq-articles__title"><?php echo $current_tax->name; ?></h3>

                    <?php
                    //get main tax
                    $main_terms = get_terms( array(
                        'taxonomy' => 'faq_main_tax',
                        'hide_empty' => true,
                    ));

                    // get tax and show post for this tax
                    $terms = get_terms( array(
                        'taxonomy' => 'faq_tax',
                        'hide_empty' => true,
                    ));
                    $terms_slug = [];

                    if ( !is_wp_error($terms) ) {
                        foreach ($terms as $term) {
                            // get posts from taxonomies
                            $args = array(
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'faq_main_tax',
                                        'field'    => 'slug',
                                        'terms'    => $current_tax->slug,
                                    ),
                                    array(
                                        'taxonomy' => 'faq_tax',
                                        'field'    => 'slug',
                                        'terms'    => $term->slug,
                                    ),
                                ),
                                'post_type' => 'faq',
                                'posts_per_page' => -1
                            );
                            $posts = get_posts( $args );
                            if ( $posts ) {
                            ?>

                            <div class="faq-articles-block">
                                <h3 class="faq-articles-block__title"><?php echo $term->name ?></h3>
                                <div class="faq-articles-wrapper">
                                    <ul class="faq-list">
                                     <?php foreach( $posts as $post ) { ?>
                                            <li class="faq-item">
                                                <a href="<?php echo get_the_permalink($post->ID); ?>" class="faq-item__link">
                                                    <?php echo $post->post_title ?> 

                                                    <?php if ( carbon_get_post_meta( $post->ID, 'faq_video_oembed' ) ) : ?>
                                                        <i class="fas fa-video icon-videocam"></i>
                                                    <?php endif; ?>
                                                </a>
                                            </li>
                                     <?php } ?>

                                    </ul>
                                 </div>
                            </div>
                           <?php
                            }//end if empty
                        } //end term foreach
                     } // endif ?>

                </div>
                <div class="sidebar-container faq-sidebar-container">
                    <div class="faq-question-list">

                    <?php
                        if ( !is_wp_error($main_terms) ) {
                            foreach ($main_terms as $term) {
                                $active = '';
                                if ( $current_tax->slug == $term->slug ) {
                                    $active = 'active';
                                }
                                $color = carbon_get_term_meta( $term->term_id, 'color_faq_main_tax' ); 
                                $icon  = carbon_get_term_meta( $term->term_id, 'icon_faq_main_tax' ); ?>

                                <a id="icon_<?php echo $term->term_id; ?>" href="<?php echo get_term_link($term) ?>" class="faq-question-item <?php echo $active; ?>">
                                    
                                    <?php if ( !empty($icon) && get_post_mime_type($icon) == 'image/svg+xml' ) : 

                                        if ( !empty($color) ) :
                                            echo '<style>#icon_'.$term->term_id.' path {fill: '.$color.';}</style>';
                                        endif;

                                        $svg_file = file_get_contents(wp_get_attachment_image_url($icon, 'full'));
                                        echo $svg_file;

                                    elseif ( !empty($icon) && get_post_mime_type($icon) == 'image/png' ) : 

                                        echo '<img src="'.wp_get_attachment_image_url($icon, 'full').'" alt="icon">';

                                    endif; ?>
                                    
                                    <h3 class="faq-question-item__title"><?php echo $term->name ?></h3>
                                </a>

                        <?php }
                        }
                    ?>

<!--                        <a href="#" class="faq-question-item">-->
<!--                            <img class="faq-question-item__img" src="img/info-circle.png"/>-->
<!--                            <h3 class="faq-question-item__title">Working with the platform</h3>-->
<!--                        </a>-->
<!--                        <a href="#" class="faq-question-item">-->
<!--                            <i class="faq-question-item__icon fas fa-gavel"></i>-->
<!--                            <h3 class="faq-question-item__title">Dispute resolution</h3>-->
<!--                        </a>-->
<!--                        <a href="#" class="faq-question-item">-->
<!--                            <img class="faq-question-item__img" src="img/smt-token.png"/>-->
<!--                            <h3 class="faq-question-item__title">Payments and SMT Token</h3>-->
<!--                        </a>-->
<!--                        <a href="#" class="faq-question-item">-->
<!--                            <img class="faq-question-item__img faq-question-item__img--security" src="img/security.png"/>-->
<!--                            <h3 class="faq-question-item__title">Security</h3>-->
<!--                        </a>-->
<!--                        <a href="#" class="faq-question-item">-->
<!--                            <img class="faq-question-item__img" src="img/smt-token.png"/>-->
<!--                            <h3 class="faq-question-item__title">Security</h3>-->
<!--                        </a>-->
                    </div>
                    <div class="faq-help-side">
                        <h3 class="faq-help-side__title"><?php echo __('Still need help?', 'market'); ?></h3>
                        <span onclick="parent.LC_API.open_chat_window({source:'minimized'}); return false" class="faq-help-side__btn">
                            <?php echo __('Contact manager', 'market'); ?>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer('white');