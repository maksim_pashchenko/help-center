<?php
/**
 * Template name: Video & Webinars
 */

get_header('white');

$paged = get_query_var('paged') ? get_query_var('paged') : 1;
$args = array(
    'posts_per_page' => 6,
    'paged' => $paged,
    'orderby' => 'date',
    'order' => 'DESC',
    'post_type' => 'video'
);

$video_loop = new WP_Query($args);

// Pagination fix
$temp_query = $wp_query;
$wp_query = NULL;
$wp_query = $video_loop;
//TODO: add modal, style pagination
?>

    <div class="page-content">
        <div class="container">
            <div class="description-page">
                <h1><?php the_title(); ?></h1>
                <?php the_content(); ?>
            </div>

            <div class="video-items-wrap">

                <?php if ($video_loop->have_posts()) :

                    while ($video_loop->have_posts()) : $video_loop->the_post();

                        $video_url = carbon_get_post_meta(get_the_ID(), 'video_oembed');
                        $video_id = parse_video_uri($video_url)['id'];
                        $video_thumbnail = $video_id ? get_video_thumbnail_uri($video_url) : '';

                        if (!empty($video_url)) : ?>

                            <div class="video-item">
                                <a video-url="<?php echo $video_url; ?>" class="item-photo open-popup-video">
                                    <div class="play-video">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="83" height="59" viewBox="0 0 83 59">
                                            <defs>
                                                <style>
                                                    .cls-1 {
                                                        opacity: 0.9;
                                                    }
                                                </style>
                                            </defs>
                                            <g id="Монтажная_область_1" data-name="Монтажная область 1">
                                                <image id="Слой_2" data-name="Слой 2" class="cls-1" width="83" height="59" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFMAAAA7CAYAAAAHFbY3AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsSAAALEgHS3X78AAAAB3RJTUUH4ggbCw8B3PpIzgAAA5FJREFUeNrtnE9oHFUcxz87+zZ/WlNsa7pt6b9jQ4gHIUUIKUJLewjSQi7qWRC8SQ8NifQinooePRQF8eJJRAVBvEigLb300hYi9KCibdSa1qbNJt3k/Xp4b7qTTbo2ydTfzsv7wJf3ZnZYvvvl7c7OvHm/0uDgIGugF9gJ9Hi9kNGWjLqBLq9Orw6g4lvj+2XfLwNJpk2Aklcz4mW9ljLtom/rvv/I9x8BC17zXjWvOeCh12ymvQ/8A/zdKpCRkZEnfSMirY7dBZwGhoDDwG4fwmahDvwJTAGXgK/99qq0CvMM8C5ulG1WKsA+r+PAWeAT4PxqB68WZg/wJfCK9idpQzqB94BjwBvAveyLzWF2AD8Ah7Rdtzkv43J6Dfe7C6wM8wIxyGdlP/AZ8Fa6o1ytVtP+q8C4tsOCcWhqaupKX1/fb7B8ZL6v7aygnANOAJQGBgbAfbUvabsqMEdHR0dvJn7jdW03BecUgLHWAgxruyk4w8BHpf7+fgNcB7ZpOyowc0C/sdbuIwa5UbYABxJgr7aTQNhjRKS68feJAHuMtbJb20UgVA3IS9ouAqHXWGt3aLsIhB0JsF3bRSC8aEQk/i3Khx5jrd2q7SIQuhM297REnnQba22XtotA6DQidGi7CISKAdlMU7fPk4qx1pa1XQRCOdn4e0RS0pvDkRyIIzNH/utZo8gaiGHmSAwzR2KYORLDzBEjIkvaJgJhyYhIXdtFINRjmPlRNyIyr+0iEBaMiMxpuwiEeSMiD7VdBELNiMistotAmE2Au9ouAuGuEZEZbReBMGNE5I62i0C4Y0RkWttFIEzHMPNj2ohwW9tFINw2IL/jlgPHZ47WTw34Nb02v4ZbBh1ZH9fHxsYWjL+dOUkMcyNMAiS+sMB3jQIDUevQN9C40/4LcJW4xnw93BgfH78Jy6ctPgS+0nZWQD5IO9mHEC4DP2o7KxiTExMTk+lG84TaO8BPwAFtlwXgFvB2dodpmpycx62d/gI4ou22jbkGvAk8yO5MVjkz3Qc5DXIO5I82OFO2k2ogH4OcBJlx+xqYFtPmnwKf40bqENAHHMQVijLaQ+N/ooarY/QzcBH4FvjraQeb5nSbWAS+9wJ3wurFFYvajrsETStubcWtbu2iUXErrbTViasRVGFltS1Do9pWtuJW2i/ROFGmQwRaV9xaZGXFrbTqVg33czbH8mpbD7xmvf4FZljDzfPH0wm97k2b1cIAAAAASUVORK5CYII="/>
                                                <image id="Слой_3" data-name="Слой 3" x="31" y="16" width="26" height="27" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAbCAQAAADIH1fgAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfiCBsLDwHc+kjOAAAA0ElEQVQ4y5XULUiDYRQG0HcqCAYRwSA220yC2MS0sGY3myyCyRVBm0GTaDMYbLqwaTWvrWoxCJbxBcMwjWMwKAxkz735wPvz3Fu0NZWsC7i3kaGBn7q1Njmq/Na11Rwxcm45RTB0aiFFUDkymyJ4d5AjeLWXI+jbzRH07OQInjVyBI+2TdVUZbFkdTwTgpdyWbrJ8d7sZ3f6cGg6eb2Blrnknz6dmE8S8eXMUpK9kQsrWcqv/p/gcXSjnk3unfVsGz3YzPbek61s73X+Tspk/Q2F2bdsEXSOjQAAAABJRU5ErkJggg=="/>
                                            </g>
                                        </svg>
                                    </div>

                                    <img src="<?php echo $video_thumbnail; ?>" alt="image">
                                </a>

                                <div class="video-item-description">
                                    <div class="item-info">

                                        <?php if ($video_categories = get_the_terms(get_the_ID(), 'video_cat')) :

                                            foreach ($video_categories as $category) :

                                                $category_color = carbon_get_term_meta($category->term_id, 'color_video_cat');
                                                $category_link = get_category_link($category->term_id); ?>

                                                <a class="item-categories" href="<?php echo $category_link; ?>"
                                                   style="background: <?php echo $category_color; ?>">
                                                    <?php echo $category->name; ?>
                                                </a>

                                            <?php endforeach;
                                        endif; ?>

                                        <div class="item-date"><?php echo get_the_time('M j, Y', get_the_ID()); ?></div>
                                        <!-- /.item-date -->
                                    </div>
                                    <div class="item-title">
                                        <a>
                                            <?php echo get_the_title(); ?>
                                        </a>
                                    </div>
                                    <!-- /.item-title -->
                                </div>
                                <!-- /.item-description -->
                            </div>

                        <?php endif;

                    endwhile;

                endif;
                wp_reset_postdata(); ?>

            </div>
            <!-- /.vireo-items-wrap -->

            <?php $pagination_args = array(
                'screen_reader_text' => null,
            );

            the_posts_pagination($pagination_args); ?>

        </div>
        <!-- /.container -->
    </div>
    <!-- /.page-content -->

<?php
// Reset main query object
$wp_query = NULL;
$wp_query = $temp_query;

get_footer('white');
